package org.mayun.server.linzeyv;

import java.util.HashMap;
import java.util.List;

import org.mayun.dao.linzeyv.ProjectDaoImpl;
import org.mayun.utils.SqlHelper;

public class listHotProjectServiceImpl {

	ProjectDaoImpl projectDao = new ProjectDaoImpl();

	public List<HashMap<String, Object>> listHotProject(String userid) {
		return projectDao.listHotProject(userid);

	}

}
