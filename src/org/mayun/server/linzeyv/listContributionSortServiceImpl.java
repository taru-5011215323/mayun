package org.mayun.server.linzeyv;

import java.util.HashMap;
import java.util.List;

import org.mayun.dao.linzeyv.ProjectDaoImpl;

public class listContributionSortServiceImpl {
	
	ProjectDaoImpl projectDao = new ProjectDaoImpl();

	public List<HashMap<String, Object>> listContributionSort() {
		return projectDao.listContributionSort();

	}

}
