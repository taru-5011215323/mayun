package org.mayun.server.liyu;

import java.util.HashMap;

import org.mayun.dao.liyu.getProjectForkWatchStartDO;
import org.mayun.dao.liyu.getSnippetForkWatchStartDO;
/**
 * 查询是否点赞收藏关注
 * @author liyu
 *
 */
public class ForkWatchStart {
	/**
	 * 查询项目
	 * @param user_id
	 * @param ProjectID
	 * @return
	 */
	public HashMap<String,Object> project(String user_id,String ProjectID) {
		getProjectForkWatchStartDO getProjectForkWatchStartDO = new getProjectForkWatchStartDO();
		return getProjectForkWatchStartDO.projectDetai(user_id, ProjectID);
	}
	/**
	 * 查询代码片段
	 * @param user_id
	 * @param ProjectID
	 * @return
	 */
	public HashMap<String,Object> snippet(String user_id,String ProjectID) {
		getSnippetForkWatchStartDO getSnippetForkWatchStartDO = new getSnippetForkWatchStartDO();
		return getSnippetForkWatchStartDO.projectDetai(user_id, ProjectID);
	}
}
