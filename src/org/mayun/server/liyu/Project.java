package org.mayun.server.liyu;

import java.util.HashMap;
import java.util.List;

import org.mayun.dao.liyu.delectProjectDO;
import org.mayun.dao.liyu.delectProjectJoinDO;
import org.mayun.dao.liyu.getProjectDetailDO;
import org.mayun.dao.liyu.getSnippetDetailDO;
import org.mayun.dao.liyu.getUserDO;
import org.mayun.dao.liyu.insertProjectJoinDO;
import org.mayun.dao.liyu.listProjectForkDO;
import org.mayun.dao.liyu.listProjectIsJoinDO;
import org.mayun.dao.liyu.listProjectJoinDO;
import org.mayun.dao.liyu.listProjectStartDO;
import org.mayun.dao.liyu.listProjectWatchDO;
import org.mayun.dao.liyu.updateProjectDO;
import org.mayun.dao.liyu.updateProjectUserDO;
/**
 * 项目详情页业务层
 * @author liyu
 *
 */
public class Project {
	
	/**
	 * 
	 * @param PrjectId
	 * @return 一个实体项目（HashMap）
	 */
	public HashMap<String,Object> getProjectDetail(String ProjectId) {
		getProjectDetailDO g= new getProjectDetailDO();
		return g.projectDetai(ProjectId);
	}
	/**
	 * 
	 * @param project_id
	 * @param language_id
	 * @param category_id
	 * @param project_is_public
	 * @param project_name
	 * @param project_exploit
	 * @param project_comment
	 * @param project_version
	 * @return
	 */
	public int updateProject(String project_id,String language_id,String category_id,String project_path,String project_name,int project_exploit,String project_comment) {
		updateProjectDO updateprojectDO = new updateProjectDO ();
		return updateprojectDO.update_project_content(project_id, language_id, category_id, project_path, project_name, project_exploit, project_comment);
	}
	/**
	 * 删除项目
	 * @param project_id
	 * @return
	 */
	public int delectProject(String project_id) {
		delectProjectDO delectprojectDO = new delectProjectDO ();
		return delectprojectDO.update_project_content(project_id);
	}
	/**
	 * 查询收藏
	 * @param ProjectId
	 * @return 收藏用户
	 */
	public List<HashMap<String,Object>> listProjectWatch(String ProjectId) {
		listProjectWatchDO g= new listProjectWatchDO();
		return g.ProjectWatch(ProjectId);
	}
	/**
	 * 查询关注
	 * @param ProjectId
	 * @return 关注用户
	 */
	public List<HashMap<String,Object>> listProjectFork(long ProjectId) {
		listProjectForkDO g= new listProjectForkDO();
		return g.ProjectFork(ProjectId);
	}
	/**
	 * 查询点赞
	 * @param ProjectId
	 * @return 点赞用户
	 */
	public List<HashMap<String,Object>> listProjectStart(String ProjectId) {
		listProjectStartDO g= new listProjectStartDO();
		return g.ProjectStart(ProjectId);
	}

	/**
	 * 删除参与者
	 * @param project_id
	 * @param user_id
	 * @return
	 */
	public int delectProjectJoin(long project_id,String user_id) {
		delectProjectJoinDO updateprojectUser= new delectProjectJoinDO();
		return updateprojectUser.delectProjectJoin(user_id, project_id);
	}
	/**
	 * 查询项目所有参与者
	 * @param project_id
	 * @return
	 */
	public  List<HashMap<String,Object>> listProjectJoin(String project_id) {
		listProjectJoinDO listprojectJoin = new listProjectJoinDO ();
		return listprojectJoin.selectProjectJoin(project_id);
	}
	/**
	 * 添加参与者
	 * @param project_id
	 * @param 邮箱或者手机号或者地址
	 * @return
	 */
	public int insertProjectJoin(String project_id,String user) {
		String user_id=listProjectIsJoin (user,project_id);
		if(user_id==null) {
			System.out.println("1");
			return 0;
		}else {
			System.out.println(user_id);
			insertProjectJoinDO insertProjectJoin = new insertProjectJoinDO();
			return insertProjectJoin.insertProjectJoin(user_id, project_id);
		}
	}
	/**
	 * 将用户属性转成id
	 * @param project_id
	 * @return
	 */
	public  String listProjectIsJoin(String user,String projectid) {
		getUserDO getUser= new getUserDO();
		String userid=String.valueOf(getUser.User(user).get("user_id")) ;
		listProjectIsJoinDO listProjectIsJoin = new listProjectIsJoinDO (); 
			if(listProjectIsJoin.selectProjectIsJoin(userid, projectid).size()>0) {
				return null;
			}else {
				return userid;
			}
		
	}
	/**
	 * 转移用户
	 * @param project_id
	 * @param project_user
	 * @return 
	 */
	public int updataProjectUser(String project_id,String user) {
		String user_id=listProjectIsJoin(user,project_id);
		if(user_id==null) {
			System.out.println("1");
			return 0;
		}else {
			System.out.println(user_id);
			updateProjectUserDO updateprojectUser = new updateProjectUserDO ();
			return updateprojectUser.updateProjectUser(project_id, user_id);
		}
	}
	
	
}
