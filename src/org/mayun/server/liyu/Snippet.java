package org.mayun.server.liyu;

import java.util.HashMap;
import java.util.List;

import org.mayun.dao.liyu.delectSnippetDO;
import org.mayun.dao.liyu.getSnippetDetailDO;
import org.mayun.dao.liyu.listProjectForkDO;
import org.mayun.dao.liyu.listProjectStartDO;
import org.mayun.dao.liyu.listProjectWatchDO;
import org.mayun.dao.liyu.listSnippetForkDO;
import org.mayun.dao.liyu.listSnippetStartDO;
import org.mayun.dao.liyu.listSnippetWatchDO;
import org.mayun.dao.liyu.updateProjectDO;
import org.mayun.dao.liyu.updateSnippetDO;
/**
 * 代码片段详情页
 * @author liyu
 *
 */
public class Snippet {
	/**
	 * 代码片段查询
	 * @param SnippetId
	 * @return 代码项目实体
	 */
	public HashMap<String,Object> getSnippetDetail(String SnippetId) {
		getSnippetDetailDO s= new getSnippetDetailDO();
		return s.projectDetai(SnippetId);
	}
	/**
	 * 代码片段编辑
	 * @param project_id
	 * @param language_id
	 * @param category_id
	 * @param project_is_public
	 * @param project_name
	 * @param project_comment
	 * @return
	 */
	public int updateSnippet(String project_id,String language_id,String category_id,String project_is_public,String project_name,String project_comment,String snippet_label,String snippet_intro) {
		updateSnippetDO updatesnippetDO = new updateSnippetDO ();
		return updatesnippetDO.update_snippet_content(project_id, language_id, category_id, project_is_public, project_name, project_comment,snippet_label,snippet_intro);
	}
	/**
	 * 代码片段删除
	 * @param snippet_id
	 * @return
	 */
	public int delectSnippet(String snippet_id) {
		delectSnippetDO delectsnippetDO = new delectSnippetDO ();
		return delectsnippetDO.update_snippet_content(snippet_id);
	}
	/**
	 * 查询收藏
	 * @param SnippetId
	 * @return 收藏用户
	 */
	public List<HashMap<String,Object>> listSnippetWatch(String SnippetId) {
		listSnippetWatchDO g= new listSnippetWatchDO();
		return g.SnippetWatch(SnippetId);
	}
	/**
	 * 查询关注
	 * @param SnippetId
	 * @return 关注用户
	 */
	public List<HashMap<String,Object>> listSnippetFork(String SnippetId) {
		listSnippetForkDO g= new listSnippetForkDO();
		return g.SnippetFork(SnippetId);
	}
	/**
	 * 查询点赞
	 * @param SnippetId
	 * @return 点赞用户
	 */
	public List<HashMap<String,Object>> listSnippetStart(String SnippetId) {
		listSnippetStartDO g= new listSnippetStartDO();
		return g.SnippetStart(SnippetId);
	}
	
}
