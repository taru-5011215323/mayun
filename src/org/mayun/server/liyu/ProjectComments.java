package org.mayun.server.liyu;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.mayun.common.Comments;
import org.mayun.dao.liyu.getProjectDetailDO;
import org.mayun.dao.liyu.insertProjectCommentsDO;
import org.mayun.dao.liyu.insertProjectReplyDO;
import org.mayun.dao.liyu.listProjectCommentsDO;
import org.mayun.dao.liyu.listProjectReplyDO;
import org.mayun.utils.IdBuild;
import org.mayun.utils.UIdBuild;
/**
 * 评论及回复
 * @author liyu
 *
 */
public class ProjectComments {
	/**
	 * 
	 * @param projectid
	 * @return 有关这个项目的全部评论及回复
	 */
	public List<Comments> Comments(long projectid) {
		listProjectCommentsDO comment=new listProjectCommentsDO();
		List<HashMap<String,Object>> list=comment.projectComments(projectid);
		List<Comments> comments=new ArrayList<Comments>();
		if(list.size()>0) {			
			for(HashMap<String,Object> map:list) {
				Comments com =new Comments();
				com.setId(map.get("project_comments_id")==null?null:map.get("project_comments_id").toString());
				com.setUserId(map.get("comments_user_id")==null?null:map.get("comments_user_id").toString());
				com.setUserName(map.get("user_name")==null?null:map.get("user_name").toString());
				com.setComments(map.get("project_comments_comment")==null?null:map.get("project_comments_comment").toString());
				com.setList(Reply(map.get("project_comments_id")==null?null:map.get("project_comments_id").toString()));
				comments.add(com);				
			}
		}
		
		return comments;
	}
	/**
	 * 
	 * @param target_id
	 * @return 递归显示回复
	 */
	public List<Comments> Reply(String target_id) {
			listProjectReplyDO reply = new listProjectReplyDO();
			List<HashMap<String,Object>> list=reply.projectDetai(target_id);
			List<Comments> comments=new ArrayList<Comments>();
			if(list.size()>0) {
				for(HashMap<String,Object> map:list) {
					Comments com =new Comments();
					com.setId(map.get("project_reply_id")==null?null:map.get("project_reply_id").toString());
					com.setUserId(map.get("project_reply_user_id")==null?null:map.get("project_reply_user_id").toString());
					com.setUserName(map.get("user_name")==null?null:map.get("user_name").toString());
					com.setComments(map.get("project_reply_comment")==null?null:map.get("project_reply_comment").toString());
					com.setList(Reply(map.get("project_reply_id")==null?null:map.get("project_reply_id").toString()));
					comments.add(com);
				}
				return comments;
			}else {
				return null;
			}
	}
	/**
	 * 插入评论
	 * @param CommentUserId
	 * @param ProjectId
	 * @param Comment
	 * @return 
	 */
	public int insertProjectComments(String CommentUserId,String ProjectId,String Comment) {
		String CommentId=UIdBuild.getUId();
		insertProjectCommentsDO insertprojectCommentsDO =new insertProjectCommentsDO();
		return insertprojectCommentsDO.projectDetai(CommentId, CommentUserId, ProjectId, Comment);
	}
	/**
	 * 插入回复
	 * @param ReplyUserId
	 * @param CommentId
	 * @param Comment
	 * @return
	 */
	public int insertProjectReply(String ReplyUserId,String CommentId,String Comment) {
		String ReplyId=UIdBuild.getUId();
		insertProjectReplyDO insertprojectReplyDO =new insertProjectReplyDO();
		return insertprojectReplyDO.projectDetai(ReplyId, ReplyUserId, CommentId, Comment);
	}
	
	
}
