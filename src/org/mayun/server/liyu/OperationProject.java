package org.mayun.server.liyu;

import java.util.HashMap;
import java.util.List;

import org.mayun.dao.liyu.listOperationProjectDO;
import org.mayun.dao.liyu.listProjectForkDO;

/**
 * 操作表
 * @author liyu
 *
 */
public class OperationProject {
	/**
	 * 根据操作名和项目Id查同类操作
	 * @param OperationProjectId
	 * @param operation_name
	 * @return 返回这个项目的这种操作
	 */
	public List<HashMap<String,Object>> listOperationProject(String OperationProjectId,String operation_name) {
		listOperationProjectDO g= new listOperationProjectDO();
		return g.OperationProjectUpdatetime(OperationProjectId, operation_name);
	}
}
