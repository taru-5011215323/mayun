package org.mayun.server.liyu;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.mayun.common.Comments;
import org.mayun.dao.liyu.getProjectDetailDO;
import org.mayun.dao.liyu.insertProjectCommentsDO;
import org.mayun.dao.liyu.insertProjectReplyDO;
import org.mayun.dao.liyu.insertSnippetCommentsDO;
import org.mayun.dao.liyu.insertSnippetReplyDO;
import org.mayun.dao.liyu.listProjectCommentsDO;
import org.mayun.dao.liyu.listProjectReplyDO;
import org.mayun.dao.liyu.listSnippetCommentsDO;
import org.mayun.dao.liyu.listSnippetReplyDO;
import org.mayun.utils.IdBuild;
import org.mayun.utils.UIdBuild;
/**
 * 评论及回复
 * @author liyu
 *
 */
public class SnippetComments {
	/**
	 * 
	 * @param projectid
	 * @return 有关这个项目的全部评论及回复
	 */
	public List<Comments> Comments(String snippetid) {
		listSnippetCommentsDO comment=new listSnippetCommentsDO();
		List<HashMap<String,Object>> list=comment.SnippetDetai(snippetid);
		List<Comments> comments=new ArrayList<Comments>();
		if(list.size()>0) {			
			for(HashMap<String,Object> map:list) {
				Comments com =new Comments();
				com.setId(map.get("snippet_comments_id")==null?null:map.get("snippet_comments_id").toString());
				com.setUserId(map.get("snippet_comments_user_id")==null?null:map.get("snippet_comments_user_id").toString());
				com.setUserName(map.get("user_name")==null?null:map.get("user_name").toString());
				com.setComments(map.get("snippet_comments_comments")==null?null:map.get("snippet_comments_comments").toString());
				com.setList(Reply(map.get("snippet_comments_id")==null?null:map.get("snippet_comments_id").toString()));
				comments.add(com);				
			}
		}
		
		return comments;
	}
	/**
	 * 
	 * @param target_id
	 * @return 递归显示回复
	 */
	public List<Comments> Reply(String target_id) {
			listSnippetReplyDO reply = new listSnippetReplyDO();
			List<HashMap<String,Object>> list=reply.SnippetDetai(target_id);
			List<Comments> comments=new ArrayList<Comments>();
			if(list.size()>0) {
				for(HashMap<String,Object> map:list) {
					Comments com =new Comments();
					com.setId(map.get("snippet_reoly_id")==null?null:map.get("snippet_reoly_id").toString());
					com.setUserId(map.get("snippet_reply_user_id")==null?null:map.get("snippet_reply_user_id").toString());
					com.setUserName(map.get("user_name")==null?null:map.get("user_name").toString());
					com.setComments(map.get("snippet_reply_comment")==null?null:map.get("snippet_reply_comment").toString());
					com.setList(Reply(map.get("snippet_reoly_id")==null?null:map.get("snippet_reoly_id").toString()));
					comments.add(com);
				}
				return comments;
			}else {
				return null;
			}
	}
	/**
	 * 插入评论
	 * @param CommentUserId
	 * @param ProjectId
	 * @param Comment
	 * @return 
	 */
	public int insertSnippetComments(String CommentUserId,String SnippetId,String Comment) {
		String CommentId=UIdBuild.getUId();
		insertSnippetCommentsDO insertsnippetCommentsDO =new insertSnippetCommentsDO();
		return insertsnippetCommentsDO.insertSnippetComments(CommentId, CommentUserId, SnippetId, Comment);
	}
	/**
	 * 插入回复
	 * @param ReplyUserId
	 * @param CommentId
	 * @param Comment
	 * @return
	 */
	public int insertSnippetReply(String ReplyUserId,String ReplyFollowId,String Comment) {
		String ReplyId=UIdBuild.getUId();
		insertSnippetReplyDO insertSnippetReply =new insertSnippetReplyDO();
		return insertSnippetReply.projectDetai(ReplyId, ReplyUserId, ReplyFollowId, Comment);
	}
	
	
}
