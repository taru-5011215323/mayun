package org.mayun.server.hjl;

import org.mayun.dao.hjl.ProjectupdateDao;

public class ProjectupdateImpl {
	public int update_project_comment(String project_id,String language_id,String category_id,String project_is_public,String project_name,String project_exploit,String project_comment,String project_version) {
		ProjectupdateDao project=new ProjectupdateDao();
		return project.update_project_content(project_id, language_id, category_id, project_is_public, project_name, project_exploit, project_comment, project_version);
	}
	public int update_project_adress(String adress,String project_id) {
		ProjectupdateDao project=new ProjectupdateDao();
		return project.update_project_adress(adress, project_id);
	}
}
