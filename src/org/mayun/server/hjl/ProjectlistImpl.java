package org.mayun.server.hjl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.mayun.dao.hjl.ProjectlistDao;

public class ProjectlistImpl {
	/**
	 * 
	 * @param user_id
	 * @param scope
	 * @param sort
	 * @return
	 */
	public List<HashMap<String,Object>> project_list(String user_id,String scope,String sort,int page,long size){
		ProjectlistDao project=new ProjectlistDao();
		List<HashMap<String, Object>> list=null;
		if(sort==null) {
			sort="project_updatetime DESC";
		}else if(sort.equals("created_at")) {
			sort="project_creattime DESC";
		}else if(sort.equals("created")) {
			sort="project_creattime";
		}else if(sort.equals("name")){
			sort="project_name  desc";
		}
		if(scope==null||scope.equals("full")) {
			list=project.list_project_full(user_id,sort,page,size);
		}else if(scope.equals("public")) {
			list=project.list_project_public(user_id,sort,page);
		}else if(scope.equals("private")) {
			list=project.list_project_private(user_id,sort,page);
		}else if(scope.equals("persion")) {
			list=project.list_project_create(user_id,sort,page);
		}else if(scope.equals("join")) {
			list=project.list_project_join(user_id, sort,page);
		}else if(scope.equals("fork")) {
			list=project.list_project_fork(user_id,sort,page);
		}
		System.out.println(sort);
		UsergetImpl user=new UsergetImpl();
		HashMap<String,Object> l=user.userGet(user_id).get(0);
		String na=l.get("user_name")==null?"0":l.get("user_name").toString();
		if(list.size()>0) {
			for(HashMap<String,Object> map:list) {
				String watch=map.get("watch")==null?"0":map.get("watch").toString();
				String star=map.get("star")==null?"0":map.get("star").toString();
				String fork=map.get("fork")==null?"0":map.get("fork").toString();
				String uid=map.get("user_id")==null?"0":map.get("user_id").toString();
				String uname=map.get("user_name")==null?"0":map.get("user_name").toString();
				if(!uid.equals(user_id)) {
					map.put("project_label","fork from "+uname);
					map.put("user_name",na);
				}
				if(watch.equals("0")) {
					map.put("watch", 0);
				}
				if(star.equals("0")) {
					map.put("star", 0);
				}
				if(fork.equals("0")) {
					map.put("fork", 0);
				}
			}
		}
		return list;
	}
	public List<HashMap<String,Object>> project_search(String user_id,String com){
		ProjectlistDao project=new ProjectlistDao();
		List<HashMap<String, Object>> list=project.list_project_search(user_id, com);
		System.out.println(list.size());
		return list;
	}
}
