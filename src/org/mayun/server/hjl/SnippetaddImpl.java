package org.mayun.server.hjl;

import org.mayun.dao.hjl.SnippetinsertDao;

public class SnippetaddImpl {
	public int snippetInsert(long user_id,String language_id,String category_id,String snippet_label,String snippet_name,String snippet_intro,String snippet_comment,String snippet_is_public) {
		SnippetinsertDao snippet=new SnippetinsertDao();
		return snippet.insert_snippet_create(user_id, language_id, category_id, snippet_label, snippet_name, snippet_intro, snippet_comment, snippet_is_public);
	}
}
