package org.mayun.server.hjl;

import java.util.HashMap;
import java.util.List;

import org.mayun.dao.hjl.SnippetlistDao;

public class SnippetlistImpl {
	public List<HashMap<String,Object>> snippet_list(String user_id,String code,int pageNum,long size){
		SnippetlistDao snippet=new SnippetlistDao();
		List<HashMap<String,Object>> list=null;
		if(code==null||code.equals("full")) {
			list=snippet.list_snippet_full(user_id,pageNum,size);
		}else if(code.equals("public")) {
			list=snippet.list_snippet_public(user_id,pageNum,size);
		}else if(code.equals("private")) {
			list=snippet.list_snippet_private(user_id,pageNum,size);
		}else if(code.equals("fork")) {
			list=snippet.list_snippet_fork(user_id,pageNum,size);
		}else if(code.equals("start")) {
			list=snippet.list_snippet_start(user_id,pageNum,size);
		}
		if(list.size()>0) {
			for(HashMap<String,Object> map:list) {
				String star=map.get("star")==null?"0":map.get("star").toString();
				String fork=map.get("fork")==null?"0":map.get("fork").toString();
				if(star.equals("0")) {
					map.put("star", 0);
				}
				if(fork.equals("0")) {
					map.put("fork", 0);
				}
			}
		}
		return list;
	}
}
