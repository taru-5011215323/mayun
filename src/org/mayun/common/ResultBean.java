package org.mayun.common;

public class ResultBean<T> {
	String status;
	String message;
	T data;
	public ResultBean(String status, String message, T data) {
		this.status = status;
		this.message = message;
		this.data = data;
	}
	public ResultBean(String status, String message) {
		this.status = status;
		this.message = message;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public T getData() {
		return data;
	}
	public void setData(T data) {
		this.data = data;
	}
	
}
