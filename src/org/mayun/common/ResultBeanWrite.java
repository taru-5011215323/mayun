package org.mayun.common;

import javax.servlet.http.HttpServletResponse;
import com.google.gson.Gson;

public class ResultBeanWrite {
	public  static void writer(HttpServletResponse response,Object result) throws Exception{
		Gson g=new Gson();
		String json=g.toJson(result);
		response.setCharacterEncoding("utf-8");
		response.setContentType("application/json");
		response.getWriter().println(json);
		response.getWriter().close();
	}
}
