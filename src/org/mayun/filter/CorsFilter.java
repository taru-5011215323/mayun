package org.mayun.filter;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
/**
 * 同源请求过滤器
 * @author hasee
 *
 */
@WebFilter("/*")
public class CorsFilter implements Filter{

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		((HttpServletResponse) response).setHeader("Access-Control-Allow-Origin",((HttpServletRequest) request).getHeader("Origin"));
		((HttpServletResponse) response).setHeader("Access-Control-Allow-Methods", "POST,GET,OPTIONS,DELETE");
		((HttpServletResponse) response).setHeader("Access-Control-Max-Age", "3600");
		((HttpServletResponse) response).setHeader("Access-Control-Allow-Headers", "x-requested-with,Content-Type");
		((HttpServletResponse) response).setHeader("Access-Control-Allow-Credentials","true");
		System.out.println("CorsFilter方法");
		
		chain.doFilter(request,response);
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub
		
	}

}
