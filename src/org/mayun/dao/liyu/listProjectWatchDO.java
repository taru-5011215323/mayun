package org.mayun.dao.liyu;

import java.util.HashMap;
import java.util.List;

import org.mayun.utils.SqlHelper;
/**
 * 查询收藏此项目的用户
 * @author liyu
 *
 */
public class listProjectWatchDO {
	/**
	 * 
	 * @param ProjectID
	 * @return 收藏此项目的所有用户
	 */
	public List<HashMap<String,Object>>  ProjectWatch(String ProjectID){
		String sql="select u.* from project_watch p \r\n" + 
				"left join user u on p.user_id=u.user_id\r\n" + 
				"where project_id=?";
		List<HashMap<String,Object>> list= SqlHelper.select(sql,ProjectID);
		return list;
	}
}
