package org.mayun.dao.liyu;



import java.util.HashMap;
import java.util.List;

import org.mayun.utils.SqlHelper;
import org.mayun.utils.TimeBuild;

public class snippetForkDao {
	public boolean snippetForkDao(long user_id ,String  snippet) {
		String sql="INSERT INTO snippet_fork ( user_id, snippet_id, fork_time, recently_time )VALUES( ?, ?,?,? )";
		int i=SqlHelper.update(sql, user_id,snippet,TimeBuild.Time(),TimeBuild.Time());
		if(i>0) {
			return true;
		}
		return false;
	}
}
