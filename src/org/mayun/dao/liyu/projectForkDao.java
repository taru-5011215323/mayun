package org.mayun.dao.liyu;



import org.mayun.utils.SqlHelper;
import org.mayun.utils.TimeBuild;

public class projectForkDao {
	public boolean projectForkDao(long user_id ,String  project_id) {
		String sql="INSERT INTO project_fork (user_id,project_id,fork_time,recently_time) VALUES (?,?,?,?)";
		int i=SqlHelper.update(sql, user_id,project_id,TimeBuild.Time(),TimeBuild.Time());
		if(i>0) {
			return true;
		}
		return false;
	}
}
