package org.mayun.dao.liyu;

import java.util.HashMap;
import java.util.List;

import org.mayun.utils.SqlHelper;
/**
 * 查询各项时间及项目
 * @author liyu
 *
 */
public class listOperationProjectDO {
	/**
	 * 
	 * @param OperationProjectId
	 * @param operation_name
	 * @return 时间排序显示完整的操作信息和项目信息
	 */
	public List<HashMap<String,Object>> OperationProjectUpdatetime(String OperationProjectId,String operation_name){
		String sql="select * from operation o \r\n" + 
				"left join project p on o.operation_project_id=p.project_id\r\n" + 
				"where o.operation_project_id=? and operation_name=?\r\n" + 
				"order by o.operation_datatime desc";
		System.out.println(OperationProjectId);
		System.out.println(operation_name);
		List<HashMap<String,Object>> list= SqlHelper.select(sql,OperationProjectId,operation_name);
		System.out.println(list.size());
		return list;
	}
}
