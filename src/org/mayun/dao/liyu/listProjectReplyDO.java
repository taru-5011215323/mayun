package org.mayun.dao.liyu;

import java.util.HashMap;
import java.util.List;

import org.mayun.utils.SqlHelper;

/**
 * 查询评论回复
 * @author liyu
 *
 */
public class listProjectReplyDO {
		/**
		 * 
		 * @param CommentsID
		 * @return 返回回复数组
		 */
		public List<HashMap<String,Object>>  projectDetai(String CommentsID){
			String sql="select p.*,u.user_name from project_reply p \r\n" + 
					"left join user u on p.project_reply_user_id=u.user_id\r\n" +  
					"where project_reply_target_id=?";
			List<HashMap<String,Object>> list= SqlHelper.select(sql,CommentsID);
			return list;
		}

}
