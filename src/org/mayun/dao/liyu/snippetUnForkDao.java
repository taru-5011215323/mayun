package org.mayun.dao.liyu;



import java.util.HashMap;
import java.util.List;

import org.mayun.utils.SqlHelper;
import org.mayun.utils.TimeBuild;

public class snippetUnForkDao {
	public boolean snippetUnForkDao(long user_id ,String  snippet) {
		String sql="DELETE FROM snippet_fork WHERE user_id=? AND snippet_id=?";
		int i=SqlHelper.update(sql, user_id,snippet);
		if(i>0) {
			return true;
		}
		return false;
	}
}
