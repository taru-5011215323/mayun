package org.mayun.dao.liyu;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.mayun.utils.SqlHelper;

/**
 * �༭����Ƭ��
 * @author liyu
 *
 */
public class updateSnippetDO {
	/**
	 * 
	 * @param snippet_id
	 * @param language_id
	 * @param category_id
	 * @param snippet_is_public
	 * @param snippet_name
	 * @param snippet_comment
	 * @return
	 */
		public int update_snippet_content(String project_id,String language_id,String category_id,String project_is_public,String project_name,String project_comment,String snippet_label,String snippet_intro) {
			String sql="update snippet set language_id=?,category_id=?,snippet_is_public=?,snippet_name=?,snippet_comment=?,snippet_label=?,snippet_intro=? where snippet_id=?";
			return SqlHelper.update(sql,language_id,category_id,project_is_public,project_name,project_comment,snippet_label,snippet_intro,project_id);
		}	
}
