package org.mayun.dao.liyu;

import java.util.HashMap;
import java.util.List;

import org.mayun.utils.SqlHelper;

/**
 * 项目详情表
 * @author liyu
 *
 */
public class getSnippetDetailDO {
	/**
	 * 
	 * @param SnippetID
	 * @return 完整代码片段信息及用户名、语言、类别
	 */
	public HashMap<String,Object>  projectDetai(String SnippetID){
		String sql="select s.*,u.user_name,l.language_name,c.category_name,f.fork,w.watch,r.start\r\n" + 
				"				from snippet s\r\n" + 
				"				left join user u on  u.user_id=s.user_id \r\n" + 
				"				left join language l on l.language_id=s.language_id\r\n" + 
				"				left join category  c on c.category_id=s.category_id\r\n" +
				"left join (select COUNT(*) as watch,snippet_id from snippet_watch group by  snippet_id) w on s.snippet_id=w.snippet_id\r\n" + 
				"left join (select COUNT(*) as fork,snippet_id from snippet_fork group by  snippet_id) f on s.snippet_id=f.snippet_id\r\n" + 
				"left join (select COUNT(*) as start,snippet_id from snippet_start group by  snippet_id) r on s.snippet_id=r.snippet_id\r\n" +
				"				where s.snippet_id=? and snippet_status=1";
		List<HashMap<String,Object>> list= SqlHelper.select(sql,SnippetID);
		return list.get(0);
	}
	
	
}