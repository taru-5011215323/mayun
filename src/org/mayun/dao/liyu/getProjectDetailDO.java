package org.mayun.dao.liyu;

import java.util.HashMap;
import java.util.List;

import org.mayun.utils.SqlHelper;

/**
 * 项目详情表
 * @author liyu
 *
 */
public class getProjectDetailDO {
	/**
	 * 
	 * @param ProjectID
	 * @return 完整项目信息及用户名、语言、类别
	 */
	public HashMap<String,Object>  projectDetai(String ProjectID){
		String sql="select p.*,u.user_name,l.language_name,c.category_name,f.fork,w.watch,s.start\r\n" + 
				"from project p \r\n" + 
				"left join user u on  u.user_id=p.user_id\r\n" + 
				"left join language l on l.language_id=p.language_id\r\n" + 
				"left join category  c on c.category_id=p.category_id\r\n" +
				"left join (select COUNT(*) as watch,project_id from project_watch group by  project_id) w on p.project_id=w.project_id\r\n" + 
				"left join (select COUNT(*) as fork,project_id from project_fork group by  project_id) f on p.project_id=f.project_id\r\n" + 
				"left join (select COUNT(*) as start,project_id from project_start group by  project_id) s on p.project_id=s.project_id\r\n" + 
				"where p.project_id=? and p.project_status=1";
		List<HashMap<String,Object>> list= SqlHelper.select(sql,ProjectID);
		return list.get(0);
	}
	
	
}
