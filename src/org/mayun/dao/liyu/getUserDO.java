package org.mayun.dao.liyu;

import java.util.HashMap;
import java.util.List;

import org.mayun.utils.SqlHelper;
import org.mayun.utils.getLoginNameType;

/**
 * 用户
 * @author liyu
 *
 */
public class getUserDO {
	/**
	 * 
	 * @param ProjectID
	 * @return 用户实体
	 */
	public HashMap<String,Object>  User(String user){
		String type=getLoginNameType.getNameType(user);
		String sql="select * from user  where "+type+"= ?";
		List<HashMap<String,Object>> list= SqlHelper.select(sql,user);
		return list.get(0);
	}
	
	
}
