package org.mayun.dao.liyu;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.mayun.utils.SqlHelper;

/**
 * �޸���Ŀ
 * @author liyu
 *
 */
public class updateProjectDO {
	/**
	 * 
	 * @param project_id
	 * @param language_id
	 * @param category_id
	 * @param project_is_public
	 * @param project_name
	 * @param project_exploit
	 * @param project_comment
	 * @param project_version
	 * @return
	 */
		public int update_project_content(String project_id,String language_id,String category_id,String project_path,String project_name,int project_exploit,String project_comment) {
			String sql="update project set language_id=?,category_id=?,project_path=?,project_name=?,project_comment=?,project_exploit=? where project_id=?";
			return SqlHelper.update(sql,language_id,category_id,project_path,project_name,project_comment,project_exploit,project_id);
		}	
}
