package org.mayun.dao.liyu;



import org.mayun.utils.SqlHelper;
import org.mayun.utils.TimeBuild;

public class projectWatchDao {
	public boolean projectWatchDao(long user_id ,String  project_id) {
		String sql="INSERT INTO project_watch ( user_id, project_id, watch_time, recently_time )VALUES( ?, ?, ?, ? )";
		int i=SqlHelper.update(sql, user_id,project_id,TimeBuild.Time(),TimeBuild.Time());
		if(i>0) {
			return true;
		}
		
		return false;
	}
}
