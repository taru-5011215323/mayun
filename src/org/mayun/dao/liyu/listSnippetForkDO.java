package org.mayun.dao.liyu;

import java.util.HashMap;
import java.util.List;

import org.mayun.utils.SqlHelper;
/**
 * 查询点赞此代码片段的用户
 * @author liyu
 *
 */
public class listSnippetForkDO {
	/**
	 * 
	 * @param ProjectID
	 * @return 点赞此代码片段的所有用户
	 */
	public List<HashMap<String,Object>>  SnippetFork(String ProjectID){
		String sql="select u.* from snippet_fork s \r\n" + 
				"left join user u on s.user_id=u.user_id\r\n" + 
				"where snippet_id=?";
		List<HashMap<String,Object>> list= SqlHelper.select(sql,ProjectID);
		return list;
	}
}
