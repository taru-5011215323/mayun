package org.mayun.dao.liyu;

import java.util.HashMap;
import java.util.List;

import org.mayun.utils.SqlHelper;

/**
 * 查询评论回复
 * @author liyu
 *
 */
public class listSnippetReplyDO {
		/**
		 * 
		 * @param CommentsID
		 * @return 返回回复数组
		 */
		public List<HashMap<String,Object>>  SnippetDetai(String CommentsID){
			String sql="select s.*,u.user_name from snippet_reply s \r\n" + 
					"					left join user u on s.snippet_reply_user_id=u.user_id  \r\n" + 
					"					where snippet_target_id=?";
			List<HashMap<String,Object>> list= SqlHelper.select(sql,CommentsID);
			return list;
		}
		

}
