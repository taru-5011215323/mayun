package org.mayun.dao.liyu;

import java.util.HashMap;
import java.util.List;

import org.mayun.utils.SqlHelper;

/**
 * ��������
 * @author liyu
 *
 */
public class insertSnippetCommentsDO {
	/**
	 * 
	 * @param CommentId
	 * @param CommentUserId
	 * @param SnippetId
	 * @param Comment
	 * @return
	 */
	public int  insertSnippetComments(String CommentId,String CommentUserId,String SnippetId,String Comment){
		String sql="insert into snippet_comments values(?,?,?,?)";
		int row= SqlHelper.update(sql, CommentId,CommentUserId,SnippetId,Comment);
		return row;
	}
	
}