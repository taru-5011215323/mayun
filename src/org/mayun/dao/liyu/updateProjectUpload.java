package org.mayun.dao.liyu;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.mayun.utils.SqlHelper;
/**
 * 更新版本
 * @author liyu
 *
 */
public class updateProjectUpload {
	SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
	/**
	 * 
	 * @param project_id
	 * @param project_version
	 * @return
	 */
	public int ProjectUpload(String project_id,String project_version) {
		String project_updatetime=df.format(new Date());
		project_version=String.valueOf(Integer.valueOf(project_version)+1);
		String sql="update snippet set project_version=?,project_updatetime=? where project_id=?";
		return SqlHelper.update(sql,project_version,project_updatetime,project_id);
	}
}
