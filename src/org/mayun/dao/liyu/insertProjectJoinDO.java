package org.mayun.dao.liyu;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.mayun.utils.SqlHelper;

/**
 * 添加参与者
 * @author liyu
 *
 */
public class insertProjectJoinDO {
	SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
	/**
	 * 
	 * @param user_id
	 * @param project_id
	 * @return
	 */
		public int insertProjectJoin(String user_id,String project_id) {
			int permis_id=1;
			String partic_time=df.format(new Date());
			String partic_update_time=df.format(new Date());
			String sql="insert into project_join values(?,?,?,?,?)";
			return SqlHelper.update(sql,user_id,project_id,permis_id,partic_time,partic_update_time);
		}	
}
