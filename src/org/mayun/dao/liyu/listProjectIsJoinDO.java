package org.mayun.dao.liyu;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.mayun.utils.SqlHelper;

/**
 * 查询是否参与者
 * @author liyu
 *
 */
public class listProjectIsJoinDO {
	/**
	 * 
	 * @param project_id
	 * @return
	 */
		public  List<HashMap<String,Object>> selectProjectIsJoin(String userid,String projectid) {
			String sql="select p.*,u.user_name from project_join p \r\n" + 
					"left join user u on p.user_id=u.user_id\r\n" + 
					"where p.project_id= ? and u.user_id= ?";
			return SqlHelper.select(sql,projectid,userid);
		}	
}
