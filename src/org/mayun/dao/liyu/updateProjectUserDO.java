package org.mayun.dao.liyu;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.mayun.utils.SqlHelper;

/**
 * 将项目转移给指定用户
 * @author liyu
 *
 */
public class updateProjectUserDO {
		/**
		 * 
		 * @param project_id
		 * @param project_user
		 * @return
		 */
		public int updateProjectUser(String project_id,String project_user) {
			String sql1="update project p set p.user_id=?," + 
					"p.project_path=(select user_adress from user where user_id=?) \r\n" + 
					"where project_id=?";
			String sql2="delete from project_join where user_id =? and project_id=?";
			return SqlHelper.update(sql1,project_user,project_user,project_id)+SqlHelper.update(sql2,project_user,project_id);
		}	
}
