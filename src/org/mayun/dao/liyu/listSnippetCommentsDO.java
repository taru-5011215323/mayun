package org.mayun.dao.liyu;

import java.util.HashMap;
import java.util.List;

import org.mayun.utils.SqlHelper;

/**
 * 查询项目评论
 * @author liyu
 *
 */
public class listSnippetCommentsDO {
		/**
		 * 
		 * @param SnippetID
		 * @return 返回评论数组
		 */
		public List<HashMap<String,Object>>  SnippetDetai(String SnippetID){
			String sql="select s.*,u.user_name from snippet_comments s \r\n" + 
					"left join user u on s.snippet_comments_user_id=u.user_id\r\n" + 
					"where snippet_id=?";
			List<HashMap<String,Object>> list= SqlHelper.select(sql,SnippetID);
			return list;
		}

}
