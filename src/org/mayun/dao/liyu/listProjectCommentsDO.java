package org.mayun.dao.liyu;

import java.util.HashMap;
import java.util.List;

import org.mayun.utils.SqlHelper;

/**
 * 查询项目评论
 * @author liyu
 *
 */
public class listProjectCommentsDO {
		/**
		 * 
		 * @param ProjectID
		 * @return 返回评论数组
		 */
		public List<HashMap<String,Object>>  projectComments(long ProjectID){
			String sql="select p.*,u.user_name from project_comments p \r\n" + 
					"left join user u on p.comments_user_id=u.user_id\r\n" + 
					"where project_id=?";
			List<HashMap<String,Object>> list= SqlHelper.select(sql,ProjectID);
			return list;
		}

}
