package org.mayun.dao.liyu;

import java.util.HashMap;
import java.util.List;

import org.mayun.utils.SqlHelper;
/**
 * 查询收藏此代码片段的用户
 * @author liyu
 *
 */
public class listSnippetStartDO {
	/**
	 * 
	 * @param ProjectID
	 * @return 收藏此代码片段的所有用户
	 */
	public List<HashMap<String,Object>>  SnippetStart(String SnippetID){
		String sql="select u.* from snippet_start s \r\n" + 
				"left join user u on s.user_id=u.user_id\r\n" + 
				"where snippet_id=?";
		List<HashMap<String,Object>> list= SqlHelper.select(sql,SnippetID);
		return list;
	}
}
