package org.mayun.dao.liyu;

import java.util.HashMap;
import java.util.List;

import org.mayun.utils.SqlHelper;

/**
 * 项目详情表
 * @author liyu
 *
 */
public class getProjectForkWatchStartDO {
	/**
	 * 
	 * @param ProjectID
	 * @return 完整项目信息及用户名、语言、类别
	 */
	public HashMap<String,Object>  projectDetai(String user_id,String ProjectID){
		String sql="select f.user_id as fork,w.user_id as watch,s.user_id as staert\r\n" + 
				"				from project p\r\n" + 
				"				left join (select * from project_watch  where user_id=?) w on p.project_id=w.project_id\r\n" + 
				" 				left join (select * from project_fork   where user_id=?) f on p.project_id=f.project_id\r\n" + 
				" 				left join (select * from project_start  where user_id=?) s on p.project_id=s.project_id\r\n" + 
				"				where p.project_id=? ";
		List<HashMap<String,Object>> list= SqlHelper.select(sql,user_id,user_id,user_id,ProjectID);
		return list.get(0);
	}
	
	
}
