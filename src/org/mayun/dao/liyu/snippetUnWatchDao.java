package org.mayun.dao.liyu;



import java.util.HashMap;
import java.util.List;

import org.mayun.utils.SqlHelper;
import org.mayun.utils.TimeBuild;

public class snippetUnWatchDao {
	public boolean snippetUnWatchDao(long user_id ,String  snippet) {
		String sql="DELETE FROM snippet_watch WHERE user_id=? AND snippet_id=?";
		int i=SqlHelper.update(sql, user_id,snippet);
		if(i>0) {
			return true;
		}
		return false;
	}
}
