package org.mayun.dao.hjl;

import java.text.SimpleDateFormat;
import java.util.Date;
import org.mayun.utils.SqlHelper;

public class ProjectupdateDao {
	SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
	//项目管理修改
	/**
	 * 
	 * @param project_id
	 * @param language_id
	 * @param category_id
	 * @param project_is_public
	 * @param project_name
	 * @param project_exploit
	 * @param project_comment
	 * @param project_version
	 * @return
	 */
	public int update_project_content(String project_id,String language_id,String category_id,String project_is_public,String project_name,String project_exploit,String project_comment,String project_version) {
		String project_updatetime=df.format(new Date());
		String vesion=String.valueOf(Integer.valueOf(project_version)+1);
		String sql="update project set language_id=?,category_id=?,project_is_public=?,project_name=?,project_comment=?,project_exploit=?,project_version=?,project_updatetime=? where project_id=?";
		return SqlHelper.update(sql,language_id,category_id,project_is_public,project_name,project_comment,project_exploit,vesion,project_updatetime,project_id);
	}
	//导入项目
	/**
	 * 
	 * @param adress
	 * @param project_id
	 * @return
	 */
	public int update_project_adress(String adress,String project_id) {
		String sql="update project set project_path=? where project_id=?";
		return SqlHelper.update(sql, adress,project_id);
	}
}
