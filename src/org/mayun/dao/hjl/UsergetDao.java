package org.mayun.dao.hjl;

import java.util.HashMap;
import java.util.List;

import org.mayun.utils.SqlHelper;

public class UsergetDao {
	public List<HashMap<String,Object>> get_user(String userId){
		List<HashMap<String,Object>> list=null;
		String sql="select u.user_id,u.vip_id,u.user_name,u.user_email,u.user_creattime,u.user_comment,ul.follow,uf.folloing from user u left join (select count(*) follow,u.user_id from user u left join user_follower ul on u.user_id=ul.user_id where u.user_id in (select user_id from user_follower) group by user_id) ul on u.user_id=ul.user_id   left join (select count(*) folloing,u.user_id from user u left join user_follower ul on u.user_id=ul.use_user_id where u.user_id in (select user_id from user_follower) group by user_id) uf on u.user_id=uf.user_id where u.user_id=?";
		list=SqlHelper.select(sql, userId);
		return list;
	}
}