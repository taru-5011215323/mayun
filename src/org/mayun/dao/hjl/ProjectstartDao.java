package org.mayun.dao.hjl;

import java.util.HashMap;
import java.util.List;
import org.mayun.utils.SqlHelper;

public class ProjectstartDao {
	//查询与本人有关的项目的点赞
	/**
	 * 
	 * @param project_id
	 * @param user_id
	 * @return
	 */
	public List<HashMap<String, Object>> get_project_start_count(String project_id,String user_id) {
		String sql="select count(*) as start from project_start where project_id=? and user_id=? group by project_id";
		return SqlHelper.select(sql, project_id,user_id);
	}
}
