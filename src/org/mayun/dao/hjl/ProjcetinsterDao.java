package org.mayun.dao.hjl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.mayun.utils.IdBuild;
import org.mayun.utils.SqlHelper;
import org.mayun.utils.UIdBuild;

public class ProjcetinsterDao {
	SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
	//新建项目接口
	/**
	 * 
	 * @param user_id
	 * @param language_id
	 * @param category_id
	 * @param project_is_public
	 * @param project_name
	 * @param project_path
	 * @param project_comment
	 * @return
	 */
	public int insert_project_create(long user_id,String language_id,String category_id,String project_is_public,String project_name,String project_path,String project_comment) {
		String project_id=UIdBuild.getUId();
		String project_exploit="1";
		String project_veesion="0";
		String project_creattime=df.format(new Date());
		String project_updatetime=df.format(new Date());
		System.out.println(project_is_public);
		String sql="INSERT INTO project ( project_id, user_id, language_id, category_id, project_is_public, project_name, project_path, project_comment, project_exploit, project_version, project_creattime, project_updatetime ) VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";
		return SqlHelper.update(sql,project_id,user_id,language_id,category_id,project_is_public,project_name,project_path,project_comment,project_exploit,project_veesion,project_creattime,project_updatetime);
	}
}
