package org.mayun.dao.hjl;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import org.mayun.utils.SqlHelper;

public class ProjectDao {
	SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
	//项目版本号查询接口
	/**
	 * 
	 * @param project_id
	 * @return
	 */
		public String get_project_version(long project_id) {
			String sql="select project_version from project where project_id=? and project_status=1";
			List<HashMap<String,Object>> list=SqlHelper.select(sql, project_id);
			String version=null;
			if(list.size()>0) {
				HashMap<String,Object> map=list.get(0);
				version=map.get("project_version")==null?null:map.get("project_version").toString();
			}
			return version;
		}
}
