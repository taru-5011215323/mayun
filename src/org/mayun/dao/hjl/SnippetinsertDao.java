package org.mayun.dao.hjl;

import java.text.SimpleDateFormat;
import java.util.Date;
import org.mayun.utils.IdBuild;
import org.mayun.utils.SqlHelper;
import org.mayun.utils.UIdBuild;

public class SnippetinsertDao {
	SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
	//新建代码片段接口
	/**
	 * 
	 * @param user_id
	 * @param language_id
	 * @param category_id
	 * @param snippet_label
	 * @param snippet_name
	 * @param snippet_intro
	 * @param snippet_comment
	 * @param snippet_is_public
	 * @return
	 */
	public int insert_snippet_create(long user_id,String language_id,String category_id,String snippet_label,String snippet_name,String snippet_intro,String snippet_comment,String snippet_is_public) {
		String snippet_id=UIdBuild.getUId();
		String snippet_datetime=df.format(new Date());
		String sql="insert into snippet(snippet_id,user_id,language_id,category_id,snippet_label,snippet_is_public,snippet_intro,snippet_name,snippet_comment,snippet_datetime) values(?,?,?,?,?,?,?,?,?,?)";
		return SqlHelper.update(sql,snippet_id,user_id,language_id,category_id,snippet_label,snippet_is_public,snippet_intro,snippet_name,snippet_comment,snippet_datetime);
	}
}
