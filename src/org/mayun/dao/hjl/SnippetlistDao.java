package org.mayun.dao.hjl;

import java.util.HashMap;
import java.util.List;

import org.mayun.utils.SqlHelper;

public class SnippetlistDao {
	//查询本人收藏的代码片段
	/**
	 * 
	 * @param user_id
	 * @return
	 */
	public List<HashMap<String, Object>> list_snippet_fork(String user_id,int pageNum,long size) {
		String sql="select p.* ,w.comm,s.star,f.fork from(\r\n" + 
				"select s.*,language.language_name,category.category_name,public_status.public_name from ( select snippet_id,language_id,category_id,snippet_name,snippet_is_public,snippet_label,snippet_intro,snippet_datetime from snippet where snippet.snippet_id in (select snippet_id from snippet_fork where user_id=? ) )s  left join language on s.language_id=language.language_id  left join category on category.category_id=s.category_id left join public_status on public_status.public_id=s.snippet_is_public) p 	LEFT JOIN ( SELECT count( * ) AS comm, snippet_id FROM snippet_comments  GROUP BY snippet_id ) w ON p.snippet_id = w.snippet_id\r\n" + 
				"	LEFT JOIN ( SELECT count( * ) AS star, snippet_id FROM snippet_fork GROUP BY snippet_id ) s ON p.snippet_id = s.snippet_id\r\n" + 
				"	LEFT JOIN ( SELECT count( * ) AS fork, snippet_id FROM snippet_start GROUP BY snippet_id ) f ON p.snippet_id = f.snippet_id order by snippet_datetime desc limit ?,10";
		return SqlHelper.select(sql,user_id,pageNum);
	}
	//查询本人点赞的代码片段
	/**
	 * 
	 * @param user_id
	 * @return
	 */
	public List<HashMap<String, Object>> list_snippet_start(String user_id,int pageNum,long size) {
		String sql="select su.*,u.user_name,l.language_name,c.category_name from ( select p.snippet_id,p.user_id,p.category_id,p.language_id,p.snippet_name,p.snippet_label,p.snippet_intro,p.snippet_datetime,s.star,f.fork from snippet p LEFT JOIN ( SELECT count( * ) AS star, snippet_id FROM snippet_fork GROUP BY snippet_id ) s ON p.snippet_id = s.snippet_id\r\n" + 
				"				LEFT JOIN ( SELECT count( * ) AS fork, snippet_id FROM snippet_start GROUP BY snippet_id ) f ON p.snippet_id = f.snippet_id  where p.snippet_id in (select snippet_id from snippet_start where user_id=?) )su left join language l on l.language_id=su.language_id left join category c on c.category_id=su.category_id left join user u on u.user_id=su.user_id order by su.snippet_datetime desc limit ?,10";
		return SqlHelper.select(sql,user_id,pageNum);
	}
	//查询本人公有的代码片段
	/**
	 * 
	 * @param user_id
	 * @return
	 */
	public List<HashMap<String, Object>> list_snippet_public(String user_id,int pageNum,long size) {
		String sql="select p.* ,w.comm,s.star,f.fork from(\r\n" + 
				"select s.*,language.language_name,category.category_name,public_status.public_name from(\r\n" + 
				"select snippet_id,language_id,category_id,snippet_name,snippet_is_public,snippet_label,snippet_intro,snippet_datetime from snippet  where user_id=?  UNION select snippet_id,language_id,category_id,snippet_name,snippet_is_public,snippet_label,snippet_intro,snippet_datetime from snippet where snippet.snippet_id in (select snippet_id from snippet_fork where user_id=? ) )s  left join language on s.language_id=language.language_id  left join category on category.category_id=s.category_id left join public_status on public_status.public_id=s.snippet_is_public) p 	LEFT JOIN ( SELECT count( * ) AS comm, snippet_id FROM snippet_comments  GROUP BY snippet_id ) w ON p.snippet_id = w.snippet_id\r\n" + 
				"	LEFT JOIN ( SELECT count( * ) AS star, snippet_id FROM snippet_fork GROUP BY snippet_id ) s ON p.snippet_id = s.snippet_id\r\n" + 
				"	LEFT JOIN ( SELECT count( * ) AS fork, snippet_id FROM snippet_start GROUP BY snippet_id ) f ON p.snippet_id = f.snippet_id where p.snippet_is_public=1 order by snippet_datetime desc limit ?,10";
		return SqlHelper.select(sql,user_id,user_id,pageNum);
	}
	//查询本人全部的代码片段
	/**
	 * 
	 * @param user_id
	 * @return
	 */
	public List<HashMap<String, Object>> list_snippet_full(String user_id,int pageNum,long size) {
		String sql="select p.* ,w.comm,s.star,f.fork from(\r\n" + 
				"select s.*,language.language_name,category.category_name,public_status.public_name from(\r\n" + 
				"select snippet_id,language_id,category_id,snippet_name,snippet_is_public,snippet_label,snippet_intro,snippet_datetime from snippet  where user_id=?  UNION select snippet_id,language_id,category_id,snippet_name,snippet_is_public,snippet_label,snippet_intro,snippet_datetime from snippet where snippet.snippet_id in (select snippet_id from snippet_fork where user_id=? ) )s  left join language on s.language_id=language.language_id  left join category on category.category_id=s.category_id left join public_status on public_status.public_id=s.snippet_is_public) p 	LEFT JOIN ( SELECT count( * ) AS comm, snippet_id FROM snippet_comments  GROUP BY snippet_id ) w ON p.snippet_id = w.snippet_id\r\n" + 
				"	LEFT JOIN ( SELECT count( * ) AS star, snippet_id FROM snippet_fork GROUP BY snippet_id ) s ON p.snippet_id = s.snippet_id\r\n" + 
				"	LEFT JOIN ( SELECT count( * ) AS fork, snippet_id FROM snippet_start GROUP BY snippet_id ) f ON p.snippet_id = f.snippet_id order by snippet_datetime desc limit ?,?";
		return SqlHelper.select(sql, user_id,user_id,pageNum,size);
	}
	//查询本人私有的代码片段
		/**
		 * 
		 * @param user_id
		 * @return
		 */
		public List<HashMap<String, Object>> list_snippet_private(String user_id,int pageNum,long size) {
			String sql="select p.* ,w.comm,s.star,f.fork from(\r\n" + 
					"select s.*,language.language_name,category.category_name,public_status.public_name from(\r\n" + 
					"select snippet_id,language_id,category_id,snippet_name,snippet_is_public,snippet_label,snippet_intro,snippet_datetime from snippet  where user_id=?  UNION select snippet_id,language_id,category_id,snippet_name,snippet_is_public,snippet_label,snippet_intro,snippet_datetime from snippet where snippet.snippet_id in (select snippet_id from snippet_fork where user_id=? ) )s  left join language on s.language_id=language.language_id  left join category on category.category_id=s.category_id left join public_status on public_status.public_id=s.snippet_is_public) p 	LEFT JOIN ( SELECT count( * ) AS comm, snippet_id FROM snippet_comments  GROUP BY snippet_id ) w ON p.snippet_id = w.snippet_id\r\n" + 
					"	LEFT JOIN ( SELECT count( * ) AS star, snippet_id FROM snippet_fork GROUP BY snippet_id ) s ON p.snippet_id = s.snippet_id\r\n" + 
					"	LEFT JOIN ( SELECT count( * ) AS fork, snippet_id FROM snippet_start GROUP BY snippet_id ) f ON p.snippet_id = f.snippet_id where p.snippet_is_public=0 order by snippet_datetime desc limit ?,10";
			return SqlHelper.select(sql, user_id,user_id,pageNum);
		}
	//搜索代码片段
		/**
		 * 
		 * @param user_id
		 * @param com
		 * @return
		 */
		public List<HashMap<String,Object>> list_snippet_search(String user_id,String com){
			String coms="%"+com+"%";
			String sql="select p.* ,w.comm,s.star,f.fork from(\r\n" + 
					"					select s.*,language.language_name,category.category_name,public_status.public_name from(\r\n" + 
					"					select snippet_id,language_id,category_id,snippet_name,snippet_is_public,snippet_label,snippet_intro,snippet_datetime from snippet  where user_id=?  UNION select snippet_id,language_id,category_id,snippet_name,snippet_is_public,snippet_label,snippet_intro,snippet_datetime from snippet where snippet.snippet_id in (select snippet_id from snippet_fork where user_id=? ) )s  left join language on s.language_id=language.language_id  left join category on category.category_id=s.category_id left join public_status on public_status.public_id=s.snippet_is_public) p 	LEFT JOIN ( SELECT count( * ) AS comm, snippet_id FROM snippet_comments  GROUP BY snippet_id ) w ON p.snippet_id = w.snippet_id \r\n" + 
					"						LEFT JOIN ( SELECT count( * ) AS star, snippet_id FROM snippet_fork GROUP BY snippet_id ) s ON p.snippet_id = s.snippet_id\r\n" + 
					"						LEFT JOIN ( SELECT count( * ) AS fork, snippet_id FROM snippet_start GROUP BY snippet_id ) f ON p.snippet_id = f.snippet_id \r\n" + 
					"where p.snippet_name like ? order by snippet_datetime desc";
			System.out.println(com);
			return SqlHelper.select(sql, user_id,user_id,coms);
		}
}
