package org.mayun.dao.hjl;

import java.util.HashMap;
import java.util.List;
import org.mayun.utils.SqlHelper;

public class SnippetcommentsDao {
	//查询与本人有关的代码的评论数
	/**
	 * 
	 * @param snippet_id
	 * @param user_id
	 * @return
	 */
	public List<HashMap<String, Object>> get_project_watch_count(String snippet_id,String user_id) {
		String sql="select count(*) from snippet left join snippet_comments on snippet.snippet_id=snippet_comments.snippet_id where snippet.user_id=? group by snippet.snippet_id";
		return SqlHelper.select(sql, snippet_id,user_id);
	}
}
