package org.mayun.dao.hjl;

import java.util.HashMap;
import java.util.List;
import org.mayun.utils.SqlHelper;

public class ProjectwatchDao {
	//查询与本人有关的项目的关注
	/**
	 * 
	 * @param project_id
	 * @param user_id
	 * @return
	 */
	public List<HashMap<String, Object>> get_project_watch_count(String project_id,String user_id) {
		String sql="select count(*) as watch from project_watch where project_id=? and user_id=? group by project_id";
		return SqlHelper.select(sql, project_id,user_id);
	}
}
