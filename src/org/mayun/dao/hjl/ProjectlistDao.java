package org.mayun.dao.hjl;

import java.util.HashMap;
import java.util.List;
import org.mayun.utils.SqlHelper;

public class ProjectlistDao {
	//查询用户创建的项目
	/**
	 * 
	 * @param user_id
	 * @return
	 */
	public List<HashMap<String, Object>> list_project_create(String user_id,String sort,int page) {
		String sql="SELECT\r\n" + 
				"	p.*,u.user_name,\r\n" + 
				"	w.watch,\r\n" + 
				"	s.star,\r\n" + 
				"	f.fork \r\n" + 
				"FROM\r\n" + 
				"	(\r\n" + 
				"	SELECT\r\n" + 
				"		project_id,user_id,\r\n" + 
				"		language.language_name,\r\n" + 
				"		category.category_name,\r\n" + 
				"		project_is_public,\r\n" + 
				"		project_name,\r\n" + 
				"		project_label,\r\n" + 
				"		project_updatetime \r\n" + 
				"	FROM\r\n" + 
				"		project\r\n" + 
				"		LEFT JOIN language ON project.language_id = language.language_id\r\n" + 
				"		LEFT JOIN category ON category.category_id = project.category_id \r\n" + 
				"	WHERE\r\n" + 
				"		user_id =?  \r\n" + 
				"	) p\r\n" + 
				"	LEFT JOIN ( SELECT count( * ) AS watch, project_id FROM project_watch GROUP BY project_id ) w ON p.project_id = w.project_id\r\n" + 
				"	LEFT JOIN ( SELECT count( * ) AS star, project_id FROM project_start GROUP BY project_id ) s ON p.project_id = s.project_id\r\n" + 
				"	LEFT JOIN ( SELECT count( * ) AS fork, project_id FROM project_fork GROUP BY project_id ) f ON p.project_id = f.project_id \r\n" + 
				"   LEFT JOIN ( SELECT user_name,user_id FROM user GROUP BY user_id ) u ON p.user_id = u.user_id \r\n" + 
				"ORDER BY\r\n" + sort+" limlt ?,10";
		return SqlHelper.select(sql, user_id,page);
	}
	//查询用户参与的项目
	/**
	 * 
	 * @param user_id
	 * @return
	 */
	public List<HashMap<String, Object>> list_project_join(String user_id,String sort,int page) {
		String sql="SELECT\r\n" + 
				"	p.*,u.user_name,\r\n" + 
				"	w.watch,\r\n" + 
				"	s.star,\r\n" + 
				"	f.fork \r\n" + 
				"FROM\r\n" + 
				"	(\r\n" + 
				"	SELECT\r\n" + 
				"		project_id,user_id,\r\n" + 
				"		language.language_name,\r\n" + 
				"		category.category_name,\r\n" + 
				"		project_is_public,\r\n" + 
				"		project_name,\r\n" + 
				"		project_label,\r\n" + 
				"		project_updatetime \r\n" + 
				"	FROM\r\n" + 
				"		project\r\n" + 
				"		LEFT JOIN language ON project.language_id = language.language_id\r\n" + 
				"		LEFT JOIN category ON category.category_id = project.category_id \r\n" + 
				"	WHERE\r\n" + 
				"		project_id IN ( SELECT project_id FROM project_join WHERE user_id = ? )\r\n" + 
				"	) p\r\n" + 
				"	LEFT JOIN ( SELECT count( * ) AS watch, project_id FROM project_watch GROUP BY project_id ) w ON p.project_id = w.project_id\r\n" + 
				"	LEFT JOIN ( SELECT count( * ) AS star, project_id FROM project_start GROUP BY project_id ) s ON p.project_id = s.project_id\r\n" + 
				"	LEFT JOIN ( SELECT count( * ) AS fork, project_id FROM project_fork GROUP BY project_id ) f ON p.project_id = f.project_id \r\n" + 
				"   LEFT JOIN ( SELECT user_name,user_id FROM user GROUP BY user_id ) u ON p.user_id = u.user_id \r\n" + 
				"ORDER BY\r\n" + 
				"	p."+sort+" limlt ?,10";
		return SqlHelper.select(sql, user_id,page);
	}
	//查询用户收藏的项目
	/**
	 * 
	 * @param user_id
	 * @return
	 */
	public List<HashMap<String, Object>> list_project_fork(String user_id,String sort,int page) {
		String sql="SELECT\r\n" + 
				"	p.*,u.user_name,\r\n" + 
				"	w.watch,\r\n" + 
				"	s.star,\r\n" + 
				"	f.fork \r\n" + 
				"FROM\r\n" + 
				"	(\r\n" + 
				"	SELECT\r\n" + 
				"		project_id,user_id,\r\n" + 
				"		language.language_name,\r\n" + 
				"		category.category_name,\r\n" + 
				"		project_is_public,\r\n" + 
				"		project_name,\r\n" + 
				"		project_label,\r\n" + 
				"		project_updatetime \r\n" + 
				"	FROM\r\n" + 
				"		project\r\n" + 
				"		LEFT JOIN language ON project.language_id = language.language_id\r\n" + 
				"		LEFT JOIN category ON category.category_id = project.category_id \r\n" + 
				"	WHERE\r\n" + 
				"		project_id IN ( SELECT project_id FROM project_fork WHERE user_id = ? )\r\n" + 
				"	) p\r\n" + 
				"	LEFT JOIN ( SELECT count( * ) AS watch, project_id FROM project_watch GROUP BY project_id ) w ON p.project_id = w.project_id\r\n" + 
				"	LEFT JOIN ( SELECT count( * ) AS star, project_id FROM project_start GROUP BY project_id ) s ON p.project_id = s.project_id\r\n" + 
				"	LEFT JOIN ( SELECT count( * ) AS fork, project_id FROM project_fork GROUP BY project_id ) f ON p.project_id = f.project_id \r\n" + 
				"   LEFT JOIN ( SELECT user_name,user_id FROM user GROUP BY user_id ) u ON p.user_id = u.user_id \r\n" + 
				"ORDER BY\r\n" + 
				"	p."+sort+" limlt ?,10";
		System.out.println(user_id);
		return SqlHelper.select(sql, user_id,page);
	}
	//查询用户公开的项目
	/**
	 * 
	 * @param user_id
	 * @return
	 */
	public List<HashMap<String, Object>> list_project_public(String user_id,String sort,int page) {
		String sql="SELECT \r\n" + 
				"	pu.*,u.user_name,\r\n" + 
				"	w.watch,\r\n" + 
				"	s.star,\r\n" + 
				"	f.fork \r\n" + 
				"from(\r\n" + 
				"SELECT\r\n" + 
				"		s.project_id,s.user_id,\r\n" + 
				"		language.language_name,\r\n" + 
				"		category.category_name,\r\n" + 
				"		s.project_is_public,\r\n" + 
				"		s.project_name,\r\n" + 
				"		s.project_label,\r\n" + 
				"		s.project_updatetime,\r\n" + 
				"		public_status.public_name\r\n" + 
				"FROM\r\n" + 
				"	(\r\n" + 
				"	SELECT\r\n" + 
				"		project_id,user_id,\r\n" + 
				"		language_id,\r\n" + 
				"		category_id,\r\n" + 
				"		project_is_public,\r\n" + 
				"		project_name,\r\n" + 
				"		project_label,\r\n" + 
				"		project_updatetime \r\n" + 
				"	FROM\r\n" + 
				"		project \r\n" + 
				"	WHERE\r\n" + 
				"		project_id IN ( SELECT project_id FROM project_join WHERE user_id =? ) UNION\r\n" + 
				"	SELECT\r\n" + 
				"		project_id,user_id,\r\n" + 
				"		language_id,\r\n" + 
				"		category_id,\r\n" + 
				"		project_is_public,\r\n" + 
				"		project_name,\r\n" + 
				"		project_label,\r\n" + 
				"		project_updatetime \r\n" + 
				"	FROM\r\n" + 
				"		project \r\n" + 
				"	WHERE\r\n" + 
				"		user_id =? \r\n" + 
				"	) s\r\n" + 
				"	LEFT JOIN public_status ON s.project_is_public = public_status.public_id \r\n" + 
				"		LEFT JOIN language ON s.language_id = language.language_id\r\n" + 
				"		LEFT JOIN category ON category.category_id = s.category_id\r\n" + 
				"		WHERE\r\n" + 
				"		s.project_is_public = 1 \r\n" + 
				"		) pu\r\n" + 
				"	LEFT JOIN ( SELECT count( * ) AS watch, project_id FROM project_watch GROUP BY project_id ) w ON pu.project_id = w.project_id\r\n" + 
				"	LEFT JOIN ( SELECT count( * ) AS star, project_id FROM project_start GROUP BY project_id ) s ON pu.project_id = s.project_id\r\n" + 
				"	LEFT JOIN ( SELECT count( * ) AS fork, project_id FROM project_fork GROUP BY project_id ) f ON pu.project_id = f.project_id \r\n" + 
				"   LEFT JOIN ( SELECT user_name,user_id FROM user ) u ON pu.user_id = u.user_id \r\n" + 
				"ORDER BY\r\n" + 
				"	pu."+sort+" limlt ?,10";
		System.out.println(user_id);
		System.out.println(sort);
		return SqlHelper.select(sql, user_id,user_id,page);
	}
	//查询用户私有的项目
	/**
	 * 
	 * @param user_id
	 * @return
	 */
	public List<HashMap<String, Object>> list_project_private(String user_id,String sort,int page) {
		String sql="select \r\n" + 
				"	pu.*,u.user_name,\r\n" + 
				"	w.watch,\r\n" + 
				"	s.star,\r\n" + 
				"	f.fork \r\n" + 
				"from(\r\n" + 
				"SELECT\r\n" + 
				"		s.project_id,s.user_id,\r\n" + 
				"		language.language_name,\r\n" + 
				"		category.category_name,\r\n" + 
				"		s.project_is_public,\r\n" + 
				"		s.project_name,\r\n" + 
				"		s.project_label,\r\n" + 
				"		s.project_updatetime,\r\n" + 
				"		public_status.public_name\r\n" + 
				"FROM\r\n" + 
				"	(\r\n" + 
				"	SELECT\r\n" + 
				"		project_id,user_id,\r\n" + 
				"		language_id,\r\n" + 
				"		category_id,\r\n" + 
				"		project_is_public,\r\n" + 
				"		project_name,\r\n" + 
				"		project_label,\r\n" + 
				"		project_updatetime \r\n" + 
				"	FROM\r\n" + 
				"		project \r\n" + 
				"	WHERE\r\n" + 
				"		project_id IN ( SELECT project_id FROM project_join WHERE user_id =? ) UNION\r\n" + 
				"	SELECT\r\n" + 
				"		project_id,user_id,\r\n" + 
				"		language_id,\r\n" + 
				"		category_id,\r\n" + 
				"		project_is_public,\r\n" + 
				"		project_name,\r\n" + 
				"		project_label,\r\n" + 
				"		project_updatetime \r\n" + 
				"	FROM\r\n" + 
				"		project \r\n" + 
				"	WHERE\r\n" + 
				"		user_id =? \r\n" + 
				"	) s\r\n" + 
				"	LEFT JOIN public_status ON s.project_is_public = public_status.public_id \r\n" + 
				"		LEFT JOIN language ON s.language_id = language.language_id\r\n" + 
				"		LEFT JOIN category ON category.category_id = s.category_id\r\n" + 
				"		WHERE\r\n" + 
				"		s.project_is_public = 0 \r\n" + 
				"		) pu\r\n" + 
				"	LEFT JOIN ( SELECT count( * ) AS watch, project_id FROM project_watch GROUP BY project_id ) w ON pu.project_id = w.project_id\r\n" + 
				"	LEFT JOIN ( SELECT count( * ) AS star, project_id FROM project_start GROUP BY project_id ) s ON pu.project_id = s.project_id\r\n" + 
				"	LEFT JOIN ( SELECT count( * ) AS fork, project_id FROM project_fork GROUP BY project_id ) f ON pu.project_id = f.project_id \r\n" + 
				"   LEFT JOIN ( SELECT user_name,user_id FROM user GROUP BY user_id ) u ON pu.user_id = u.user_id \r\n" + 
				"ORDER BY\r\n" + 
				"	pu."+sort+" limlt ?,10";
		return SqlHelper.select(sql, user_id,user_id,page);
	}
	//查询用户全部的项目
	/**
	 * 
	 * @param user_id
	 * @return
	 */
	public List<HashMap<String, Object>> list_project_full(String user_id,String sort,int page,long size) {
		String sql="SELECT\r\n" + 
				"		p.*,u.user_name, \r\n" + 
				"		w.watch,\r\n" + 
				"		s.star,\r\n" + 
				"		f.fork,\r\n" + 
				"		'fork_com'=\r\n" + 
				"			CASE\r\n" + 
				"			WHEN p.user_id!='null'  THEN '1'\r\n" + 
				"			ELSE' 0'\r\n" + 
				"			END as f\r\n" + 
				"	FROM\r\n" + 
				"		(\r\n" + 
				"		SELECT\r\n" + 
				"			project_id,user_id,\r\n" + 
				"			language.language_name,\r\n" + 
				"			category.category_name,\r\n" + 
				"			project_is_public,\r\n" + 
				"			project_name,\r\n" + 
				"			project_label, \r\n" + 
				"			project_updatetime\r\n" + 
				"		FROM\r\n" + 
				"			project \r\n" + 
				"		LEFT JOIN language ON project.language_id = language.language_id \r\n" + 
				"		LEFT JOIN category ON category.category_id = project.category_id \r\n" + 
				"		WHERE\r\n" + 
				"			project_id IN ( SELECT project_id FROM project_join WHERE user_id =? ) UNION \r\n" + 
				"				SELECT \r\n" + 
				"					project_id,user_id,\r\n" + 
				"					language.language_name,\r\n" + 
				"					category.category_name,\r\n" + 
				"					project_is_public,\r\n" + 
				"					project_name, \r\n" + 
				"					project_label, \r\n" + 
				"					project_updatetime  \r\n" + 
				"				FROM\r\n" + 
				"					project\r\n" + 
				"				LEFT JOIN language ON project.language_id = language.language_id\r\n" + 
				"				LEFT JOIN category ON category.category_id = project.category_id \r\n" + 
				"				WHERE\r\n" + 
				"					user_id = ?  UNION\r\n" + 
				"		SELECT\r\n" + 
				"			project_id,user_id,\r\n" + 
				"			language.language_name,\r\n" + 
				"			category.category_name,\r\n" + 
				"			project_is_public,\r\n" + 
				"			project_name,\r\n" + 
				"			project_label, \r\n" + 
				"			project_updatetime\r\n" + 
				"		FROM\r\n" + 
				"			project \r\n" + 
				"		LEFT JOIN language ON project.language_id = language.language_id \r\n" + 
				"		LEFT JOIN category ON category.category_id = project.category_id \r\n" + 
				"		WHERE\r\n" + 
				"			project_id IN ( SELECT project_id FROM project_fork WHERE user_id =? )\r\n" + 
				"		) p\r\n" + 
				"		LEFT JOIN ( SELECT count( * ) AS watch, project_id FROM project_watch GROUP BY project_id ) w ON p.project_id = w.project_id \r\n" + 
				"		LEFT JOIN ( SELECT count( * ) AS star, project_id FROM project_start GROUP BY project_id ) s ON p.project_id = s.project_id \r\n" + 
				"		LEFT JOIN ( SELECT count( * ) AS fork, project_id FROM project_fork GROUP BY project_id ) f ON p.project_id = f.project_id \r\n" + 
				"		LEFT JOIN ( SELECT user_name,user_id FROM user GROUP BY user_id ) u ON p.user_id = u.user_id " + 
				"ORDER BY  "+sort +" limit ?,?";
				System.out.println(user_id);
		return SqlHelper.select(sql,user_id,user_id,user_id,page,size);
	}
	//搜索项目
	/**
	 * 
	 * @param user_id
	 * @param com
	 * @return
	 */
	public List<HashMap<String,Object>> list_project_search(String user_id,String com){
		String coms="%"+com+"%";
		System.out.println(user_id);
		String sql="SELECT p.*,u.user_name,w.watch,s.star, f.fork  FROM ( SELECT project_id,language.language_name,category.category_name, project_is_public, project_name,project_label,project_updatetime 	FROM 	project	LEFT JOIN language ON project.language_id = language.language_id	LEFT JOIN category ON category.category_id = project.category_id  	WHERE project_id IN ( SELECT project_id FROM project_join WHERE user_id = ? ) UNION 	SELECT	project_id,\r\n" + 
				"language.language_name,	category.category_name, project_is_public,project_name,project_label,project_updatetime 	FROM	project	LEFT JOIN language ON project.language_id = language.language_id	LEFT JOIN category ON category.category_id = project.category_id 	WHERE 	user_id = ? \r\n" + 
				"	) p LEFT JOIN ( SELECT count( * ) AS watch, project_id FROM project_watch GROUP BY project_id ) w ON p.project_id = w.project_id	LEFT JOIN ( SELECT count( * ) AS star, project_id FROM project_start GROUP BY project_id ) s ON p.project_id = s.project_id	LEFT JOIN ( SELECT count( * ) AS fork, project_id FROM project_fork GROUP BY project_id ) f ON p.project_id = f.project_id  where p.project_name like ?	ORDER BY 	p.project_updatetime DESC";
		return SqlHelper.select(sql, user_id,user_id,coms);
	}
}
