package org.mayun.dao.hjl;

import java.util.HashMap;
import java.util.List;
import org.mayun.utils.SqlHelper;

public class ProjectforkDao {
	//查询与本人有关的项目的收藏
	/**
	 * 
	 * @param project_id
	 * @param user_id
	 * @return
	 */
	public List<HashMap<String, Object>> get_project_fork_count(String project_id,String user_id) {
		String sql="select count(*) as fork from project_fork where project_id=? and user_id=? group by project_id";
		return SqlHelper.select(sql, project_id,user_id);
	}
}
