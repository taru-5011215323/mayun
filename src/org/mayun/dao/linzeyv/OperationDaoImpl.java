package org.mayun.dao.linzeyv;

import java.util.HashMap;

import java.util.List;

import org.mayun.utils.SqlHelper;
/**
 * 对动态表一些的操作Dao包
 * @author lzy
 *
 */

public class OperationDaoImpl {
	
	/**
	 * (动态) 所有人的动态
	 * 根据时间排序显示 倒序
	 * 显示操作人和具体的操作对象
	 */
	public List<HashMap<String,Object>> listAllContributionSort() {
		
		String sql="SELECT\r\n" + 
				"	o.*,\r\n" + 
				"	u.user_name,\r\n" + 
				"	u.user_photo,\r\n" + 
				"	p.project_name,\r\n" + 
				"	s.snippet_name \r\n" + 
				"FROM\r\n" + 
				"	operation o\r\n" + 
				"	LEFT JOIN `user` u ON operation_user_id = u.user_id\r\n" + 
				"	LEFT JOIN project p ON o.operation_project_id = p.project_id\r\n" + 
				"	LEFT JOIN snippet s ON o.operation_project_id = s.snippet_id \r\n" + 
				"ORDER BY\r\n" + 
				"	operation_datatime DESC";		
		List<HashMap<String, Object>> list=SqlHelper.select(sql);
		return list;
		
	}
	/**
	 * (动态) 自己的操作动态  限制条件用户id  userid
	 * 根据时间排序显示 倒序
	 * 显示操作人和具体的操作对象
	 */
	public List<HashMap<String,Object>> listOwnContributionSort(String operationUserId) {
		
		String sql="SELECT\r\n" + 
				"	o.*,\r\n" + 
				"	u.user_name,\r\n" + 
				"	u.user_photo,\r\n" + 
				"	p.project_name,\r\n" + 
				"	s.snippet_name \r\n" + 
				"FROM\r\n" + 
				"	operation o\r\n" + 
				"	LEFT JOIN `user` u ON operation_user_id = u.user_id\r\n" + 
				"	LEFT JOIN project p ON o.operation_project_id = p.project_id\r\n" + 
				"	LEFT JOIN snippet s ON o.operation_project_id = s.snippet_id \r\n" + 
				"WHERE\r\n" + 
				"	o.operation_user_id = ? \r\n" + 
				"ORDER BY\r\n" + 
				"	operation_datatime DESC";		
		List<HashMap<String, Object>> list=SqlHelper.select(sql,operationUserId);
		return list;
		
	}
	

}
