package org.mayun.dao.linzeyv;

import java.util.HashMap;
import java.util.List;

import org.mayun.utils.SqlHelper;
/**
 * 对代码片段表的操作Dao包
 * @author lzy
 *
 */

public class snipptDaoImpl {
	/**
	 * 今日代码片段热门   
	 * 时间限制今天新建的代码片段  根据点赞数排序 
	 * 链接代码点赞表 
	 */
	public List<HashMap<String,Object>> getTodayHotSnippet() {
		
		String sql="SELECT\r\n" + 
				"s.*,\r\n" + 
				"u.user_id,\r\n" + 
				"u.user_name,\r\n" + 
				"sn.snippet_id,\r\n" + 
				"sn.snippet_name,\r\n" + 
				"sn.snippet_intro\r\n" + 
				"\r\n" + 
				"FROM\r\n" + 
				"	( SELECT snippet_id, COUNT( * ) cnt FROM snippet_start GROUP BY snippet_id ORDER BY cnt DESC ) s\r\n" + 
				"	LEFT JOIN snippet sn ON s.snippet_id = sn.snippet_id \r\n" + 
				"	LEFT JOIN `user` u  ON sn.user_id=u.user_id\r\n" + 
				"WHERE\r\n" + 
				"	DATE(snippet_datetime) = DATE(NOW())\r\n" + 
				"	LIMIT 0,5";		
		List<HashMap<String, Object>> list=SqlHelper.select(sql);
		return list;
		
	}
	/**
	 *  七日代码片段内热门      时间限制在七天内  根据点赞数排序
	 */
	public List<HashMap<String,Object>> getSevenDayHotSnippet() {
		
		String sql="SELECT\r\n" + 
				"s.*,\r\n" + 
				"u.user_id,\r\n" + 
				"u.user_name,\r\n" + 
				"sn.snippet_id,\r\n" + 
				"sn.snippet_name,\r\n" + 
				"sn.snippet_intro\r\n" + 
				"\r\n" + 
				"FROM\r\n" + 
				"	( SELECT snippet_id, COUNT( * ) cnt FROM snippet_start GROUP BY snippet_id ORDER BY cnt DESC ) s\r\n" + 
				"	LEFT JOIN snippet sn ON s.snippet_id = sn.snippet_id \r\n" + 
				"	LEFT JOIN `user` u  ON sn.user_id=u.user_id\r\n" + 
				"WHERE\r\n" + 
				"	datediff( now( ), snippet_datetime ) <= 6 \r\n" + 
				"	LIMIT 0,5";		
		List<HashMap<String, Object>> list=SqlHelper.select(sql);
		return list;
		
	}
	/**
	 * 推荐代码片段   
	 * 根据语言和类别来推荐 按更新时间排序
	 */
	public List<HashMap<String,Object>> listRecommendableProjectsSnippet(String language,String catgory,int pagenum) {
		
		String sql="SELECT\r\n" + 
				"	p.*,\r\n" + 
				"	w.watch,\r\n" + 
				"	s.star,\r\n" + 
				"	f.fork \r\n" + 
				"FROM\r\n" + 
				"	(\r\n" + 
				"	SELECT\r\n" + 
				"		snippet_id,\r\n" + 
				"		`user`.user_id,\r\n" + 
				"		`user`.user_name,\r\n" + 
				"		`user`.user_photo,\r\n" + 
				"		language.language_name,\r\n" + 
				"		category.category_name,\r\n" + 
				"		snippet_is_public,\r\n" + 
				"		snippet_name,\r\n" + 
				"		snippet_comment,\r\n" + 
				"		snippet_label,\r\n" + 
				"		snippet_intro,\r\n" + 
				"		snippet_datetime,\r\n" + 
				"	  snippet_creattime	\r\n" + 
				"	FROM\r\n" + 
				"		snippet\r\n" + 
				"		LEFT JOIN language ON snippet.language_id = language.language_id\r\n" + 
				"		LEFT JOIN category ON category.category_id = snippet.category_id \r\n" + 
				"		LEFT JOIN `user` ON snippet.user_id=`user`.user_id\r\n" + 
				"			where language.language_id like ? and category.category_id like ?\r\n" + 
				" UNION\r\n" + 
				"	SELECT\r\n" + 
				"		snippet_id,\r\n" + 
				"		`user`.user_id,\r\n" + 
				"		`user`.user_name,\r\n" + 
				"		`user`.user_photo,\r\n" + 
				"		language.language_name,\r\n" + 
				"		category.category_name,\r\n" + 
				"		snippet_is_public,\r\n" + 
				"		snippet_name,\r\n" + 
				"		snippet_comment,\r\n" + 
				"		snippet_label,\r\n" +
				"		snippet_intro,\r\n" + 
				"		snippet_datetime,\r\n" + 
				"	  snippet_creattime		\r\n" + 
				"	FROM\r\n" + 
				"		snippet\r\n" + 
				"		LEFT JOIN language ON snippet.language_id = language.language_id\r\n" + 
				"		LEFT JOIN category ON category.category_id = snippet.category_id \r\n" + 
				"		LEFT JOIN `user` ON snippet.user_id=`user`.user_id\r\n" + 
				"		where language.language_id like ? and category.category_id like ?\r\n" + 
				"		\r\n" + 
				"			\r\n" + 
				"	) p\r\n" + 
				"	LEFT JOIN ( SELECT count( * ) AS watch, snippet_id FROM snippet_watch GROUP BY snippet_id ) w ON p.snippet_id = w.snippet_id\r\n" + 
				"	LEFT JOIN ( SELECT count( * ) AS star, snippet_id FROM snippet_start GROUP BY snippet_id ) s ON p.snippet_id = s.snippet_id\r\n" + 
				"	LEFT JOIN ( SELECT count( * ) AS fork, snippet_id FROM snippet_fork GROUP BY snippet_id ) f ON p.snippet_id = f.snippet_id \r\n" + 
				"\r\n" + 
				"ORDER BY\r\n" + 
				"	p.snippet_datetime DESC\r\n"+ 
				"LIMIT ?,10";		
		List<HashMap<String, Object>> list=SqlHelper.select(sql,language,catgory,language,catgory, pagenum);
		return list;
		
	}
	/**
	 * 推荐代码片段   
	 * 根据语言和类别来推荐 按创建时间排序
	 */
	public List<HashMap<String,Object>> listcreattimeSnippet(String language,String catgory,int pagenum) {
		
		String sql="SELECT\r\n" + 
				"	p.*,\r\n" + 
				"	w.watch,\r\n" + 
				"	s.star,\r\n" + 
				"	f.fork \r\n" + 
				"FROM\r\n" + 
				"	(\r\n" + 
				"	SELECT\r\n" + 
				"		snippet_id,\r\n" + 
				"		`user`.user_id,\r\n" + 
				"		`user`.user_name,\r\n" + 
				"		`user`.user_photo,\r\n" + 
				"		language.language_name,\r\n" + 
				"		category.category_name,\r\n" + 
				"		snippet_is_public,\r\n" + 
				"		snippet_name,\r\n" + 
				"		snippet_comment,\r\n" + 
				"		snippet_label,\r\n" + 
				"		snippet_intro,\r\n" + 
				"		snippet_datetime,\r\n" + 
				"	  snippet_creattime	\r\n" + 
				"	FROM\r\n" + 
				"		snippet\r\n" + 
				"		LEFT JOIN language ON snippet.language_id = language.language_id\r\n" + 
				"		LEFT JOIN category ON category.category_id = snippet.category_id \r\n" + 
				"		LEFT JOIN `user` ON snippet.user_id=`user`.user_id\r\n" + 
				"			where language.language_id like ? and category.category_id like ?\r\n" + 
				" UNION\r\n" + 
				"	SELECT\r\n" + 
				"		snippet_id,\r\n" + 
				"		`user`.user_id,\r\n" + 
				"		`user`.user_name,\r\n" + 
				"		`user`.user_photo,\r\n" + 
				"		language.language_name,\r\n" + 
				"		category.category_name,\r\n" + 
				"		snippet_is_public,\r\n" + 
				"		snippet_name,\r\n" + 
				"		snippet_comment,\r\n" + 
				"		snippet_label,\r\n" + 
				"		snippet_intro,\r\n" + 
				"		snippet_datetime,\r\n" + 
				"	  snippet_creattime		\r\n" + 
				"	FROM\r\n" + 
				"		snippet\r\n" + 
				"		LEFT JOIN language ON snippet.language_id = language.language_id\r\n" + 
				"		LEFT JOIN category ON category.category_id = snippet.category_id \r\n" + 
				"		LEFT JOIN `user` ON snippet.user_id=`user`.user_id\r\n" + 
				"		where language.language_id like ? and category.category_id like ?\r\n" + 
				"		\r\n" + 
				"			\r\n" + 
				"	) p\r\n" + 
				"	LEFT JOIN ( SELECT count( * ) AS watch, snippet_id FROM snippet_watch GROUP BY snippet_id ) w ON p.snippet_id = w.snippet_id\r\n" + 
				"	LEFT JOIN ( SELECT count( * ) AS star, snippet_id FROM snippet_start GROUP BY snippet_id ) s ON p.snippet_id = s.snippet_id\r\n" + 
				"	LEFT JOIN ( SELECT count( * ) AS fork, snippet_id FROM snippet_fork GROUP BY snippet_id ) f ON p.snippet_id = f.snippet_id \r\n" + 
				"\r\n" + 
				"ORDER BY\r\n" + 
				"	p.snippet_creattime DESC\r\n"+ 
				"LIMIT ?,10";		
		List<HashMap<String, Object>> list=SqlHelper.select(sql,language,catgory,language,catgory, pagenum);
		return list;
		
	}

	/**
	 * 代码片段编程语言排行   cnt 单种语言个数  cotSum 全部语言个数   percentage 单种语言占所有的百分比 
	 */
	public List<HashMap<String,Object>> countLanguageSnippet() {
		
		String sql="SELECT\r\n" + 
				"	language_id,\r\n" + 
				"	language_name,\r\n" + 
				"	a.cnt,\r\n" + 
				"	b.cntSum,\r\n" + 
				"	CONCAT( ROUND( a.cnt / b.cntSum * 100, 2 ), '', '%' ) percentage \r\n" + 
				"FROM\r\n" + 
				"	(\r\n" + 
				"	SELECT\r\n" + 
				"		snippet.language_id,\r\n" + 
				"		language_name,\r\n" + 
				"		count( * ) AS cnt \r\n" + 
				"	FROM\r\n" + 
				"		snippet\r\n" + 
				"		LEFT JOIN `language` ON snippet.language_id = `language`.language_id \r\n" + 
				"	GROUP BY\r\n" + 
				"		language_id \r\n" + 
				"	ORDER BY\r\n" + 
				"		cnt DESC \r\n" + 
				"	) AS a,\r\n" + 
				"	( SELECT count( * ) AS cntSum FROM snippet ) AS b";		
		List<HashMap<String, Object>> list=SqlHelper.select(sql);
		return list;
		
	}
	/**
	 * 根据代码片段名 查询
	 * @param snippetName
	 * @return
	 */
      public List<HashMap<String,Object>> selectSnippetName(String snippetName,int pagenum) {
		
		String sql="SELECT\r\n" + 
				"	p.*,\r\n" + 
				"	w.watch,\r\n" + 
				"	s.star,\r\n" + 
				"	f.fork \r\n" + 
				"FROM\r\n" + 
				"	(\r\n" + 
				"	SELECT\r\n" + 
				"		snippet_id,\r\n" + 
				"		`user`.user_id,\r\n" + 
				"		`user`.user_name,\r\n" + 
				"		`user`.user_photo,\r\n" + 
				"		language.language_name,\r\n" + 
				"		category.category_name,\r\n" + 
				"		snippet_is_public,\r\n" + 
				"		snippet_name,\r\n" + 
				"		snippet_comment,\r\n" + 
				"		snippet_label,\r\n" + 
				"		snippet_intro,\r\n" + 
				"		snippet_datetime,\r\n" + 
				"	  snippet_creattime	\r\n" + 
				"	FROM\r\n" + 
				"		snippet\r\n" + 
				"		LEFT JOIN language ON snippet.language_id = language.language_id\r\n" + 
				"		LEFT JOIN category ON category.category_id = snippet.category_id \r\n" + 
				"		LEFT JOIN `user` ON snippet.user_id=`user`.user_id\r\n" + 
				" UNION\r\n" + 
				"	SELECT\r\n" + 
				"		snippet_id,\r\n" + 
				"		`user`.user_id,\r\n" + 
				"		`user`.user_name,\r\n" + 
				"		`user`.user_photo,\r\n" + 
				"		language.language_name,\r\n" + 
				"		category.category_name,\r\n" + 
				"		snippet_is_public,\r\n" + 
				"		snippet_name,\r\n" + 
				"		snippet_comment,\r\n" + 
				"		snippet_label,\r\n" + 
				"		snippet_intro,\r\n" + 
				"		snippet_datetime,\r\n" + 
				"	  snippet_creattime		\r\n" + 
				"	FROM\r\n" + 
				"		snippet\r\n" + 
				"		LEFT JOIN language ON snippet.language_id = language.language_id\r\n" + 
				"		LEFT JOIN category ON category.category_id = snippet.category_id \r\n" + 
				"		LEFT JOIN `user` ON snippet.user_id=`user`.user_id\r\n" + 
				"	) p\r\n" + 
				"	LEFT JOIN ( SELECT count( * ) AS watch, snippet_id FROM snippet_watch GROUP BY snippet_id ) w ON p.snippet_id = w.snippet_id\r\n" + 
				"	LEFT JOIN ( SELECT count( * ) AS star, snippet_id FROM snippet_start GROUP BY snippet_id ) s ON p.snippet_id = s.snippet_id\r\n" + 
				"	LEFT JOIN ( SELECT count( * ) AS fork, snippet_id FROM snippet_fork GROUP BY snippet_id ) f ON p.snippet_id = f.snippet_id \r\n" + 
				"WHERE\r\n" + 
				"p.snippet_name LIKE ?\r\n" + 
				"ORDER BY\r\n" + 
				"	p.snippet_creattime DESC\r\n" + 
				"	LIMIT ?,10";		
		List<HashMap<String, Object>> list=SqlHelper.select(sql,snippetName, pagenum);
		System.out.println(snippetName);
		return list;
		
	}
      /**
       * 根据代码片段名和使用的语言进行查询
       * @param language
       * @param snippetName
       * @return
       */
      public List<HashMap<String,Object>> selectLanguageSnippetName(String language ,String snippetName,int pagenum) {
  		
  		String sql="SELECT\r\n" + 
  				"	p.*,\r\n" + 
  				"	w.watch,\r\n" + 
  				"	s.star,\r\n" + 
  				"	f.fork \r\n" + 
  				"FROM\r\n" + 
  				"	(\r\n" + 
  				"	SELECT\r\n" + 
  				"		snippet_id,\r\n" + 
				"		`user`.user_id,\r\n" + 
  				"		`user`.user_name,\r\n" + 
				"		`user`.user_photo,\r\n" + 
  				"		language.language_name,\r\n" + 
  				"		category.category_name,\r\n" + 
  				"		snippet_is_public,\r\n" + 
  				"		snippet_name,\r\n" + 
  				"		snippet_comment,\r\n" + 
  				"		snippet_label,\r\n" + 
				"		snippet_intro,\r\n" + 
  				"		snippet_datetime,\r\n" + 
  				"	  snippet_creattime	\r\n" + 
  				"	FROM\r\n" + 
  				"		snippet\r\n" + 
  				"		LEFT JOIN language ON snippet.language_id = language.language_id\r\n" + 
  				"		LEFT JOIN category ON category.category_id = snippet.category_id \r\n" + 
  				"		LEFT JOIN `user` ON snippet.user_id=`user`.user_id\r\n" + 
  				"		WHERE\r\n" + 
  				"	language.language_name LIKE ? \r\n" + 
  				" UNION\r\n" + 
  				"	SELECT\r\n" + 
  				"		snippet_id,\r\n" + 
				"		`user`.user_id,\r\n" + 
  				"		`user`.user_name,\r\n" + 
				"		`user`.user_photo,\r\n" + 
  				"		language.language_name,\r\n" + 
  				"		category.category_name,\r\n" + 
  				"		snippet_is_public,\r\n" + 
  				"		snippet_name,\r\n" + 
  				"		snippet_comment,\r\n" + 
  				"		snippet_label,\r\n" + 
				"		snippet_intro,\r\n" + 
  				"		snippet_datetime,\r\n" + 
  				"	  snippet_creattime		\r\n" + 
  				"	FROM\r\n" + 
  				"		snippet\r\n" + 
  				"		LEFT JOIN language ON snippet.language_id = language.language_id\r\n" + 
  				"		LEFT JOIN category ON category.category_id = snippet.category_id \r\n" + 
  				"		LEFT JOIN `user` ON snippet.user_id=`user`.user_id\r\n" + 
  				"		WHERE\r\n" + 
  				"		language.language_name LIKE ?\r\n" + 
  				"\r\n" + 
  				"	) p\r\n" + 
  				"	LEFT JOIN ( SELECT count( * ) AS watch, snippet_id FROM snippet_watch GROUP BY snippet_id ) w ON p.snippet_id = w.snippet_id\r\n" + 
  				"	LEFT JOIN ( SELECT count( * ) AS star, snippet_id FROM snippet_start GROUP BY snippet_id ) s ON p.snippet_id = s.snippet_id\r\n" + 
  				"	LEFT JOIN ( SELECT count( * ) AS fork, snippet_id FROM snippet_fork GROUP BY snippet_id ) f ON p.snippet_id = f.snippet_id \r\n" + 
  				"WHERE p.snippet_name LIKE ?\r\n" + 
  				"ORDER BY\r\n" + 
  				"	p.snippet_creattime DESC\r\n"+ 
  				"LIMIT ?,10";		
  		List<HashMap<String, Object>> list=SqlHelper.select(sql,language,language,snippetName, pagenum);
  		return list;
  		
  	}
	

}
