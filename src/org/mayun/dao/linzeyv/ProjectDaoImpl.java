package org.mayun.dao.linzeyv;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import org.mayun.utils.SqlHelper;

/**
 * 对项目表进行操作的Dao包
 * @author lzy
 *
 */

public class ProjectDaoImpl {
	/**
	 * 热门项目 根据浏览量查询排名前五的项目    
	 * 根据用户id对项目表查询           userid
	 */
public List<HashMap<String,Object>> listHotProject(String userid) {
		
		String sql="SELECT\r\n" + 
				"	p.*,\r\n" + 
				"	w.watch,\r\n" + 
				"	s.star,\r\n" + 
				"	f.fork \r\n" + 
				"FROM\r\n" + 
				"	(\r\n" + 
				"	SELECT\r\n" + 
				"		project_id,\r\n" + 
				"		`user`.user_id,\r\n" + 
				"		`user`.user_name,\r\n" + 
				"		`user`.user_photo,\r\n" + 
				"		language.language_name,\r\n" + 
				"		category.category_name,\r\n" + 
				"		project_is_public,\r\n" + 
				"		project_name,\r\n" + 
				"		project_comment,\r\n" + 
				"		project_label,\r\n" + 
				"		project_updatetime,\r\n" + 
				"		project_creattime ,\r\n" + 
				"		project_browse\r\n" + 
				"	FROM\r\n" + 
				"		project\r\n" + 
				"		LEFT JOIN language ON project.language_id = language.language_id\r\n" + 
				"		LEFT JOIN category ON category.category_id = project.category_id \r\n" + 
				"		LEFT JOIN `user` ON project.user_id=`user`.user_id\r\n" + 
				"		WHERE  project.user_id= ?\r\n" + 
				"		AND project_id IN ( SELECT project_id FROM project_join ) UNION\r\n" + 
				"	SELECT\r\n" + 
				"		project_id,\r\n" + 
				"		`user`.user_id,\r\n" + 
				"		`user`.user_name,\r\n" + 
				"		`user`.user_photo,\r\n" + 
				"		language.language_name,\r\n" + 
				"		category.category_name,\r\n" + 
				"		project_is_public,\r\n" + 
				"		project_name,\r\n" + 
				"		project_comment,\r\n" + 
				"		project_label,\r\n" + 
				"		project_updatetime,\r\n" + 
				"		project_creattime ,\r\n" + 
				"		project_browse\r\n" + 
				"	FROM\r\n" + 
				"		project\r\n" + 
				"		LEFT JOIN language ON project.language_id = language.language_id\r\n" + 
				"		LEFT JOIN category ON category.category_id = project.category_id \r\n" + 
				"		LEFT JOIN `user` ON project.user_id=`user`.user_id\r\n" + 
				"		WHERE  project.user_id= ?\r\n" + 
				"\r\n" + 
				"	) p\r\n" + 
				"	LEFT JOIN ( SELECT count( * ) AS watch, project_id FROM project_watch GROUP BY project_id ) w ON p.project_id = w.project_id\r\n" + 
				"	LEFT JOIN ( SELECT count( * ) AS star, project_id FROM project_start GROUP BY project_id ) s ON p.project_id = s.project_id\r\n" + 
				"	LEFT JOIN ( SELECT count( * ) AS fork, project_id FROM project_fork GROUP BY project_id ) f ON p.project_id = f.project_id \r\n" + 
				"ORDER BY\r\n" + 
				"	p.project_browse DESC\r\n" + 
				"	LIMIT 0,6";		
		List<HashMap<String, Object>> list=SqlHelper.select(sql,userid,userid);
		return list;
		
	}
/**
 * （概括） 贡献度动态  
 * 根据操作时间排序 
 * 显示操作人和具体的操作对象
 */
public List<HashMap<String,Object>> listContributionSort() {
	
	String sql="SELECT\r\n" + 
			"	o.*,\r\n" + 
			"	u.user_name,\r\n" + 
			"	u.user_photo,\r\n" + 
			"	p.project_name,\r\n" + 
			"	s.snippet_name \r\n" + 
			"FROM\r\n" + 
			"	operation o\r\n" + 
			"	LEFT JOIN `user` u ON operation_user_id = u.user_id\r\n" + 
			"	LEFT JOIN project p ON o.operation_project_id = p.project_id\r\n" + 
			"	LEFT JOIN snippet s ON o.operation_project_id = s.snippet_id \r\n" + 
			"ORDER BY\r\n" + 
			"	operation_datatime DESC";		
	List<HashMap<String, Object>> list=SqlHelper.select(sql);
	return list;
	
}

/**
 * 今日热门   
 * 时间限制今天新建的项目  根据浏览量排序
 */
public List<HashMap<String,Object>> getTodayHot() {
	
	String sql="SELECT\r\n" + 
			"	p.*,\r\n" + 
			"	u.user_name	\r\n" + 
			"FROM\r\n" + 
			"	project p\r\n" + 
			"	LEFT JOIN `user` u ON p.user_id=u.user_id\r\n" + 
			"WHERE\r\n" + 
			"	DATE(project_updatetime) = DATE(NOW())\r\n" + 
			"ORDER BY\r\n" + 
			"	project_browse DESC \r\n" + 
			"	LIMIT 0,5";		
	List<HashMap<String, Object>> list=SqlHelper.select(sql);
	return list;
	
}
/**
 *  七日内热门      时间限制在七天内  根据浏览量排序
 */
public List<HashMap<String,Object>> getSevenDayHot() {
	
	String sql="SELECT\r\n" + 
			"	p.*,\r\n" + 
			"	u.user_name	\r\n" + 
			"FROM\r\n" + 
			"	project p\r\n" + 
			"	LEFT JOIN `user` u ON p.user_id=u.user_id\r\n" + 
			"WHERE\r\n" + 
			"	datediff( now( ), project_updatetime ) <= 6 \r\n" + 
			"ORDER BY\r\n" + 
			"	project_browse DESC \r\n" + 
			"	LIMIT 0,5";		
	List<HashMap<String, Object>> list=SqlHelper.select(sql);
	return list;
	
}
/**
 * 推荐项目   
 * 根据语言和类别来推荐 根据更改时间排序
 */

public List<HashMap<String,Object>> listRecommendableProjects(String language,String catgory,int pagenum) {
	
	
	String sql="SELECT\r\n" + 
			"	p.*,\r\n" + 
			"	w.watch,\r\n" + 
			"	s.star,\r\n" + 
			"	f.fork \r\n" + 
			"FROM\r\n" + 
			"	(\r\n" + 
			"	SELECT\r\n" + 
			"		project_id,\r\n" + 
			"		`user`.user_id,\r\n" + 
			"		`user`.user_name,\r\n" + 
			"		`user`.user_photo,\r\n" + 
			"		language.language_name,\r\n" + 
			"		category.category_name,\r\n" + 
			"		project_is_public,\r\n" + 
			"		project_name,\r\n" + 
			"		project_comment,\r\n" + 
			"		project_label,\r\n" + 
			"		project_updatetime,\r\n" + 
			"		project_creattime \r\n" + 
			"	FROM\r\n" + 
			"		project\r\n" + 
			"		LEFT JOIN language ON project.language_id = language.language_id\r\n" + 
			"		LEFT JOIN category ON category.category_id = project.category_id \r\n" + 
			"		LEFT JOIN `user` ON project.user_id=`user`.user_id\r\n" + 
			"	WHERE\r\n" + 
			"		language.language_id LIKE ?\r\n" + 
			"		AND category.category_id LIKE ?\r\n" + 
			"		AND project_id IN ( SELECT project_id FROM project_join ) UNION\r\n" + 
			"	SELECT\r\n" + 
			"		project_id,\r\n" + 
			"		`user`.user_id,\r\n" + 
			"		`user`.user_name,\r\n" + 
			"		`user`.user_photo,\r\n" + 
			"		language.language_name,\r\n" + 
			"		category.category_name,\r\n" + 
			"		project_is_public,\r\n" + 
			"		project_name,\r\n" + 
			"		project_comment,\r\n" + 
			"		project_label,\r\n" + 
			"		project_updatetime,\r\n" + 
			"		project_creattime \r\n" + 
			"	FROM\r\n" + 
			"		project\r\n" + 
			"		LEFT JOIN language ON project.language_id = language.language_id\r\n" + 
			"		LEFT JOIN category ON category.category_id = project.category_id \r\n" + 
			"		LEFT JOIN `user` ON project.user_id=`user`.user_id\r\n" + 
			"	WHERE\r\n" + 
			"		language.language_id LIKE ?\r\n" + 
			"		AND category.category_id LIKE ?\r\n" + 
			"	) p\r\n" + 
			"	LEFT JOIN ( SELECT count( * ) AS watch, project_id FROM project_watch GROUP BY project_id ) w ON p.project_id = w.project_id\r\n" + 
			"	LEFT JOIN ( SELECT count( * ) AS star, project_id FROM project_start GROUP BY project_id ) s ON p.project_id = s.project_id\r\n" + 
			"	LEFT JOIN ( SELECT count( * ) AS fork, project_id FROM project_fork GROUP BY project_id ) f ON p.project_id = f.project_id \r\n" + 
			"ORDER BY\r\n" + 
			"	p.project_updatetime DESC\r\n"+ 
			"LIMIT ?,10";		
	
	List<HashMap<String, Object>> list=SqlHelper.select(sql,language,catgory,language,catgory, pagenum);
	return list;
	
}
/**
 * 推荐项目   
 * 根据语言和类别来推荐 根据创建时间排序
 */

public List<HashMap<String,Object>> listcreattimeProject(String language,String catgory,int pagenum) {
	
	
	String sql="SELECT\r\n" + 
			"	p.*,\r\n" + 
			"	w.watch,\r\n" + 
			"	s.star,\r\n" + 
			"	f.fork \r\n" + 
			"FROM\r\n" + 
			"	(\r\n" + 
			"	SELECT\r\n" + 
			"		project_id,\r\n" + 
			"		`user`.user_id,\r\n" + 
			"		`user`.user_name,\r\n" + 
			"		`user`.user_photo,\r\n" + 
			"		language.language_name,\r\n" + 
			"		category.category_name,\r\n" + 
			"		project_is_public,\r\n" + 
			"		project_name,\r\n" + 
			"		project_comment,\r\n" + 
			"		project_label,\r\n" + 
			"		project_updatetime,\r\n" + 
			"		project_creattime \r\n" + 
			"	FROM\r\n" + 
			"		project\r\n" + 
			"		LEFT JOIN language ON project.language_id = language.language_id\r\n" + 
			"		LEFT JOIN category ON category.category_id = project.category_id \r\n" + 
			"		LEFT JOIN `user` ON project.user_id=`user`.user_id\r\n" + 
			"	WHERE\r\n" + 
			"		language.language_id LIKE ?\r\n" + 
			"		AND category.category_id LIKE ?\r\n" + 
			"		AND project_id IN ( SELECT project_id FROM project_join ) UNION\r\n" + 
			"	SELECT\r\n" + 
			"		project_id,\r\n" + 
			"		`user`.user_id,\r\n" + 
			"		`user`.user_name,\r\n" + 
			"		`user`.user_photo,\r\n" + 
			"		language.language_name,\r\n" + 
			"		category.category_name,\r\n" + 
			"		project_is_public,\r\n" + 
			"		project_name,\r\n" + 
			"		project_comment,\r\n" + 
			"		project_label,\r\n" + 
			"		project_updatetime,\r\n" + 
			"		project_creattime \r\n" + 
			"	FROM\r\n" + 
			"		project\r\n" + 
			"		LEFT JOIN language ON project.language_id = language.language_id\r\n" + 
			"		LEFT JOIN category ON category.category_id = project.category_id \r\n" + 
			"		LEFT JOIN `user` ON project.user_id=`user`.user_id\r\n" + 
			"	WHERE\r\n" + 
			"		language.language_id LIKE ?\r\n" + 
			"		AND category.category_id LIKE ?\r\n" + 
			"	) p\r\n" + 
			"	LEFT JOIN ( SELECT count( * ) AS watch, project_id FROM project_watch GROUP BY project_id ) w ON p.project_id = w.project_id\r\n" + 
			"	LEFT JOIN ( SELECT count( * ) AS star, project_id FROM project_start GROUP BY project_id ) s ON p.project_id = s.project_id\r\n" + 
			"	LEFT JOIN ( SELECT count( * ) AS fork, project_id FROM project_fork GROUP BY project_id ) f ON p.project_id = f.project_id \r\n" + 
			"ORDER BY\r\n" + 
			"	p.project_creattime DESC\r\n"+ 
			"LIMIT ?,10";		
	
	List<HashMap<String, Object>> list=SqlHelper.select(sql,language,catgory,language,catgory,pagenum);
	return list;
	
}

/**
 * 编程语言排行   cnt 单种语言个数  cotSum 全部语言个数   percentage 单种语言占所有的百分比 
 */
public List<HashMap<String,Object>> countLanguage() {
	
	String sql="SELECT\r\n" + 
			"	language_id,\r\n" + 
			"	language_name,\r\n" + 
			"	a.cnt,\r\n" + 
			"	b.cntSum,\r\n" + 
			"	CONCAT( ROUND( a.cnt / b.cntSum * 100, 2 ), '', '%' ) percentage \r\n" + 
			"FROM\r\n" + 
			"	(\r\n" + 
			"	SELECT\r\n" + 
			"		project.language_id,\r\n" + 
			"		language_name,\r\n" + 
			"		count( * ) AS cnt \r\n" + 
			"	FROM\r\n" + 
			"		project\r\n" + 
			"		LEFT JOIN `language` ON project.language_id = `language`.language_id \r\n" + 
			"	GROUP BY\r\n" + 
			"		language_id \r\n" + 
			"	ORDER BY\r\n" + 
			"		cnt DESC \r\n" + 
			"	) AS a,\r\n" + 
			"	( SELECT count( * ) AS cntSum FROM project ) AS b";		
	List<HashMap<String, Object>> list=SqlHelper.select(sql);
	return list;
	
}
/**
 * 根据项目名 查询
 * @param snippetName
 * @return
 */
  public List<HashMap<String,Object>> selectProjectName(String snippetName,int pagenum) {
	
	String sql="SELECT\r\n" + 
			"	p.*,\r\n" + 
			"	w.watch,\r\n" + 
			"	s.star,\r\n" + 
			"	f.fork \r\n" + 
			"FROM\r\n" + 
			"	(\r\n" + 
			"	SELECT\r\n" + 
			"		project_id,\r\n" + 
			"		`user`.user_id,\r\n" + 
			"		`user`.user_name,\r\n" + 
			"		`user`.user_photo,\r\n" + 
			"		language.language_name,\r\n" + 
			"		category.category_name,\r\n" + 
			"		project_is_public,\r\n" + 
			"		project_name,\r\n" + 
			"		project_comment,\r\n" + 
			"		project_label,\r\n" + 
			"		project_updatetime,\r\n" + 
			"		project_creattime \r\n" + 
			"	FROM\r\n" + 
			"		project\r\n" + 
			"		LEFT JOIN language ON project.language_id = language.language_id\r\n" + 
			"		LEFT JOIN category ON category.category_id = project.category_id \r\n" + 
			"		LEFT JOIN `user` ON project.user_id=`user`.user_id\r\n" + 
			"	WHERE\r\n" + 
			"	project_id IN ( SELECT project_id FROM project_join ) UNION\r\n" + 
			"	SELECT\r\n" + 
			"		project_id,\r\n" + 
			"		`user`.user_id,\r\n" + 
			"		`user`.user_name,\r\n" + 
			"		`user`.user_photo,\r\n" + 
			"		language.language_name,\r\n" + 
			"		category.category_name,\r\n" + 
			"		project_is_public,\r\n" + 
			"		project_name,\r\n" + 
			"		project_comment,\r\n" + 
			"		project_label,\r\n" + 
			"		project_updatetime,\r\n" + 
			"		project_creattime \r\n" + 
			"	FROM\r\n" + 
			"		project\r\n" + 
			"		LEFT JOIN language ON project.language_id = language.language_id\r\n" + 
			"		LEFT JOIN category ON category.category_id = project.category_id \r\n" + 
			"		LEFT JOIN `user` ON project.user_id=`user`.user_id\r\n" + 
			"\r\n" + 
			"	) p\r\n" + 
			"	LEFT JOIN ( SELECT count( * ) AS watch, project_id FROM project_watch GROUP BY project_id ) w ON p.project_id = w.project_id\r\n" + 
			"	LEFT JOIN ( SELECT count( * ) AS star, project_id FROM project_start GROUP BY project_id ) s ON p.project_id = s.project_id\r\n" + 
			"	LEFT JOIN ( SELECT count( * ) AS fork, project_id FROM project_fork GROUP BY project_id ) f ON p.project_id = f.project_id \r\n" + 
			"	WHERE\r\n" + 
			"	p.project_name LIKE ?\r\n" + 
			"ORDER BY\r\n" + 
			"	p.project_updatetime DESC\r\n"+ 
			"LIMIT ?,10";		
	List<HashMap<String, Object>> list=SqlHelper.select(sql,snippetName, pagenum);
	return list;
	
}
  /**
   * 根据项目名和使用的语言进行查询
   * @param language
   * @param snippetName
   * @return
   */
  public List<HashMap<String,Object>> selectLanguageProjectName(String language ,String snippetName,int pagenum) {
		
		String sql="SELECT\r\n" + 
				"	p.*,\r\n" + 
				"	w.watch,\r\n" + 
				"	s.star,\r\n" + 
				"	f.fork \r\n" + 
				"FROM\r\n" + 
				"	(\r\n" + 
				"	SELECT\r\n" + 
				"		project_id,\r\n" + 
				"		`user`.user_id,\r\n" + 
				"		`user`.user_name,\r\n" + 
				"		`user`.user_photo,\r\n" + 
				"		language.language_name,\r\n" + 
				"		category.category_name,\r\n" + 
				"		project_is_public,\r\n" + 
				"		project_name,\r\n" + 
				"		project_comment,\r\n" + 
				"		project_label,\r\n" + 
				"		project_updatetime,\r\n" + 
				"		project_creattime \r\n" + 
				"	FROM\r\n" + 
				"		project\r\n" + 
				"		LEFT JOIN language ON project.language_id = language.language_id\r\n" + 
				"		LEFT JOIN category ON category.category_id = project.category_id \r\n" + 
				"		LEFT JOIN `user` ON project.user_id=`user`.user_id\r\n" + 
				"	WHERE\r\n" + 
				"	language.language_name LIKE ? \r\n" + 
				"	AND\r\n" + 
				"	project_id IN ( SELECT project_id FROM project_join ) UNION\r\n" + 
				"	SELECT\r\n" + 
				"		project_id,\r\n" + 
				"		`user`.user_id,\r\n" + 
				"		`user`.user_name,\r\n" + 
				"		`user`.user_photo,\r\n" + 
				"		language.language_name,\r\n" + 
				"		category.category_name,\r\n" + 
				"		project_is_public,\r\n" + 
				"		project_name,\r\n" + 
				"		project_comment,\r\n" + 
				"		project_label,\r\n" + 
				"		project_updatetime,\r\n" + 
				"		project_creattime \r\n" + 
				"	FROM\r\n" + 
				"		project\r\n" + 
				"		LEFT JOIN language ON project.language_id = language.language_id\r\n" + 
				"		LEFT JOIN category ON category.category_id = project.category_id \r\n" + 
				"		LEFT JOIN `user` ON project.user_id=`user`.user_id\r\n" + 
				"	WHERE\r\n" + 
				"		language.language_name LIKE ?\r\n" + 
				"	) p\r\n" + 
				"	LEFT JOIN ( SELECT count( * ) AS watch, project_id FROM project_watch GROUP BY project_id ) w ON p.project_id = w.project_id\r\n" + 
				"	LEFT JOIN ( SELECT count( * ) AS star, project_id FROM project_start GROUP BY project_id ) s ON p.project_id = s.project_id\r\n" + 
				"	LEFT JOIN ( SELECT count( * ) AS fork, project_id FROM project_fork GROUP BY project_id ) f ON p.project_id = f.project_id \r\n" + 
				"	WHERE\r\n" + 
				"	p.project_name LIKE ?\r\n" + 
				"ORDER BY\r\n" + 
				"	p.project_updatetime DESC\r\n"+ 
				"LIMIT ?,10";		
		List<HashMap<String, Object>> list=SqlHelper.select(sql,language,language,snippetName, pagenum);
		return list;
		
	}



///**
// * 分页查询
// * @param language
// * @param catgory
// * @return
// */
//
//
//
//public PageModel<List> listRe(String page,String pageSize) {	
//	String sql="select p.*,l.language_name,c.category_name from project p left join language l on p.language_id=l.language_id left join category c on p.category_id=c.category_id where l.language_id like ? and c.category_id like ? order by project_updatetime";
//	PageModel  pageModel=new PageModel<>(sql,page,pageSize);	
//	List list=SqlHelper.select(pageModel.toMysqlSql());
//	pageModel.setList(list);
//	
//	List<HashMap<String, Object>> countlist=SqlHelper.select(pageModel.toCountSql());
//	
//	String total=String.valueOf(countlist.get(0).get("user_id"));
//	pageModel.setTotal(Integer.valueOf(total));
//	
//	return  pageModel;
//	
//}





	

}
