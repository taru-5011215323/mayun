package org.mayun.dao.linzeyv;

import org.mayun.utils.SqlHelper;

public class UserDaoImpl {
	/**
	 * 更改用户信息
	 * @param userid
	 * @param userrealname
	 * @param usertelephone
	 * @param userlocation
	 * @param userdetailed
	 * @return
	 */
	public int UpdateUser(String userid,String userrealname,String usertelephone,String userlocation,String userdetailed ) {
		String sql="UPDATE `user` \r\n" + 
				"SET user_real_name =?,\r\n" + 
				"user_telephone =?,\r\n" + 
				"user_location =?,\r\n" + 
				"user_detailed_address =? \r\n" + 
				"WHERE\r\n" + 
				"	user_id =?";

		return SqlHelper.update(sql,userrealname,usertelephone,userlocation,userdetailed,userid);
	}	

}
