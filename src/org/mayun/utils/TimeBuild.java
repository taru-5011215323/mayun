package org.mayun.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class TimeBuild {
	static SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
	public static String Time() {
		return df.format(new Date());
	}
}
