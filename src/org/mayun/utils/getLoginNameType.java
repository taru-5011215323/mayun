package org.mayun.utils;

import java.util.regex.Pattern;

public class getLoginNameType {
	/**
	 * 注册时验证密码
	 * @param login
	 * @return	返回登录所输入的登陆类型
	 */
	public static String  getLoginNameType(String login) {
		
		Pattern phone = Pattern.compile("^((13[0-9])|(15[^4,\\D])|(18[0,5-9]))\\d{8}$");
		Pattern email = Pattern.compile("^([a-z0-9A-Z]+[-|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$");
		
		if(phone.matcher(login).matches()) {
			if(!email.matcher(login).matches()) {
				return "user_telephone";
			}
		}
		if(email.matcher(login).matches()) {
			if(!phone.matcher(login).matches()) {
				return "user_email";
			}
		}
		
		return "user_adress";
	}
	public static String  getNameType(String login) {
		
		Pattern phone = Pattern.compile("^((13[0-9])|(15[^4,\\D])|(18[0,5-9]))\\d{8}$");
		Pattern email = Pattern.compile("^([a-z0-9A-Z]+[-|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$");
		
		if(phone.matcher(login).matches()) {
			if(!email.matcher(login).matches()) {
				return "user_telephone";
			}
		}
		if(email.matcher(login).matches()) {
			if(!phone.matcher(login).matches()) {
				return "user_email";
			}
		}
		
		return "user_name";
	}
}
