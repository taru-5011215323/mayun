package org.mayun.web.hjl;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.mayun.common.ReslutConstants;
import org.mayun.common.ResultBean;
import org.mayun.common.ResultBeanWrite;
import org.mayun.server.hjl.ProjectlistImpl;

/**
 * Servlet implementation class 搜索项目
 */
@WebServlet("/project/list/search")
public class ProjectsearchServlet extends HttpServlet {
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ResultBean result=null;
		try {
			String user_id=request.getParameter("userId");
			String com=request.getParameter("comment");
			ProjectlistImpl project=new ProjectlistImpl();
			List<HashMap<String, Object>> list=project.project_search(user_id, com);
			if(list.size()>0){
				result=new ResultBean(ReslutConstants.STATUS_SUCCESS, "查找成功",list);
			}else{
				result=new ResultBean(ReslutConstants.STATUS_UNFOUND, "查找失败");
			}
		}catch(Exception ex) {
			result=new ResultBean(ReslutConstants.STATUS_FAIL, "操作异常", ex.getMessage());
		}finally {
			try {
				ResultBeanWrite.writer(response, result);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

}
