package org.mayun.web.hjl;

import java.io.IOException;
import javax.servlet.AsyncEvent;
import javax.servlet.AsyncListener;
import javax.servlet.ServletContextAttributeEvent;
import javax.servlet.ServletContextAttributeListener;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.ServletException;
import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import javax.servlet.annotation.WebListener;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionIdListener;
import org.mayun.common.ReslutConstants;
import org.mayun.common.ResultBean;
import org.mayun.common.ResultBeanWrite;
import org.mayun.server.hjl.SnippetaddImpl;
import org.mayun.utils.SqlHelper;

/**
 * Servlet implementation class 新建代码片段
 */
@WebServlet("/snippet/insert")
public class SnippetinsertServlet extends HttpServlet {

	@Override
	protected void service (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			ResultBean result=null;
			try{
				long user_id=Long.valueOf(request.getParameter("userId"));
				String language_id=request.getParameter("languageId");
				String category_id=request.getParameter("categoryId");
				String snippet_label=request.getParameter("snippetLabel");
				String snippet_name=request.getParameter("snippetName");
				String snippet_intro=request.getParameter("snippetIntro");
				String snippet_comment=request.getParameter("snippetComment");
				String snippet_is_public=request.getParameter("snippetIsPublic");
				SnippetaddImpl snippetImpl=new SnippetaddImpl();
				int i=snippetImpl.snippetInsert(user_id, language_id, category_id, snippet_label, snippet_name, snippet_intro, snippet_comment, snippet_is_public);
				if(i>0){
					result=new ResultBean(ReslutConstants.STATUS_SUCCESS, "代码片段创建成功");
				}else{
					result=new ResultBean(ReslutConstants.STATUS_UNFOUND, "代码片段创建失败");
				}
			}catch(Exception ex){
				result=new ResultBean(ReslutConstants.STATUS_FAIL, "操作异常", ex.getMessage());
			}finally{
				try {
					ResultBeanWrite.writer(response, result);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
  }
	
}
