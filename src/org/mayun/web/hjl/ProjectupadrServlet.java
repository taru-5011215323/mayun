package org.mayun.web.hjl;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.mayun.common.ReslutConstants;
import org.mayun.common.ResultBean;
import org.mayun.common.ResultBeanWrite;
import org.mayun.server.hjl.ProjectupdateImpl;

/**
 * Servlet implementation class 项目导入
 */
@WebServlet("/project/update/adress")
public class ProjectupadrServlet extends HttpServlet {
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ResultBean result=null;
		ProjectupdateImpl project=new ProjectupdateImpl();
		try {
			String project_id=request.getParameter("projectId");
			String adress=request.getParameter("adress");
			int i=project.update_project_adress(adress, project_id);
			if(i>0){
				result=new ResultBean(ReslutConstants.STATUS_SUCCESS, "项目修改成功");
			}else{
				result=new ResultBean(ReslutConstants.STATUS_UNFOUND, "项目修改失败");
			}
		}catch(Exception e) {
			result=new ResultBean(ReslutConstants.STATUS_FAIL,e.getMessage());
		}finally {
			try {
				ResultBeanWrite.writer(response, result);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
