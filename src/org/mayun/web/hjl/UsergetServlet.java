package org.mayun.web.hjl;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.mayun.common.ReslutConstants;
import org.mayun.common.ResultBean;
import org.mayun.common.ResultBeanWrite;
import org.mayun.server.hjl.SnippetlistImpl;
import org.mayun.server.hjl.UsergetImpl;

/**
 * Servlet implementation class 查询单个用户信息
 */
@WebServlet("/user/get")
public class UsergetServlet extends HttpServlet {
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ResultBean result=null;
		try {
			String userId=request.getParameter("userId");
			UsergetImpl user=new UsergetImpl();
			List<HashMap<String,Object>> list=user.userGet(userId);
			if(list.size()>0) {
				result=new ResultBean(ReslutConstants.STATUS_SUCCESS, "搜索成功",list);
			}else {
				result=new ResultBean(ReslutConstants.STATUS_UNFOUND, "无数据");
			}
		}catch(Exception e) {
			result=new ResultBean(ReslutConstants.STATUS_FAIL, "操作异常",e.getMessage());
		}finally {
			try {
				ResultBeanWrite.writer(response, result);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

}
