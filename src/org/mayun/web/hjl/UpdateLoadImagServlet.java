package org.mayun.web.hjl;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.mayun.common.ReslutConstants;
import org.mayun.common.ResultBean;
import org.mayun.common.ResultBeanWrite;
import org.mayun.server.hjl.UpdateLoadImagServiceImpl;
import org.mayun.server.linzeyv.getSevenDayHotSnippetServiceImpl;

/**
 * 修改头像
 * @author lzy
 *
 */
@WebServlet("/api/updateloadphoto")
public class UpdateLoadImagServlet extends HttpServlet {
	@Override
	
	
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String userphoto=request.getParameter("userphoto");
		String userid=request.getParameter("userid");	
		ResultBean result =null;
		try {
			UpdateLoadImagServiceImpl serviceImol=new UpdateLoadImagServiceImpl();
			int list=serviceImol.updateloadphoto(userphoto, userid);
			if(list>0) {
				result=new ResultBean(ReslutConstants.STATUS_SUCCESS, "修改成功", list);
			}else {
				result=new ResultBean(ReslutConstants.STATUS_UNFOUND, "修改失败");
			}
		}catch(Exception ex) {
			result=new ResultBean(ReslutConstants.STATUS_FAIL, "修改异常",ex.getMessage());
		}
		
		try {
			ResultBeanWrite.writer(response, result);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}

}