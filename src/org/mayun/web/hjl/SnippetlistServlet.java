package org.mayun.web.hjl;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.mayun.common.ReslutConstants;
import org.mayun.common.ResultBean;
import org.mayun.common.ResultBeanWrite;
import org.mayun.server.hjl.SnippetlistImpl;

/**
 * Servlet implementation class 查看用户代码片段
 */
@WebServlet("/snippet/list")
public class SnippetlistServlet extends HttpServlet {
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ResultBean result=null;
		try {
			String userId=request.getParameter("userId");
			String code=request.getParameter("code");
			int pageNum=Integer.valueOf(request.getParameter("pageNum"));
			int page=0;
			long pSize=10;
			if(pageNum>0) {
				page=pageNum*10-10;
			}
			if(pageNum<0) {
				pSize=674407370;
			}
			SnippetlistImpl snippet=new SnippetlistImpl();
			List<HashMap<String,Object>> list=snippet.snippet_list(userId, code,page,pSize);
			if(list.size()>0) {
				result=new ResultBean(ReslutConstants.STATUS_SUCCESS, "搜索成功",list);
			}else {
				result=new ResultBean(ReslutConstants.STATUS_UNFOUND, "无数据");
			}
		}catch(Exception e) {
			result=new ResultBean(ReslutConstants.STATUS_FAIL, "操作异常",e.getMessage());
		}finally {
			try {
				ResultBeanWrite.writer(response, result);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
