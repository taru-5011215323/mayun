package org.mayun.web.hjl;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.mayun.common.ReslutConstants;
import org.mayun.common.ResultBean;
import org.mayun.common.ResultBeanWrite;
import org.mayun.dao.hjl.SnippetlistDao;

/**
 * Servlet implementation class 搜索代码片段
 */
@WebServlet("/snippet/list/search")
public class SnippetsearchServlet extends HttpServlet {
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ResultBean result=null;
		try {
			String user_id=request.getParameter("userId");
			String com=request.getParameter("comment");
			SnippetlistDao snippet=new SnippetlistDao();
			List<HashMap<String, Object>> list=snippet.list_snippet_search(user_id, com);
			if(list.size()>0){
				result=new ResultBean(ReslutConstants.STATUS_SUCCESS, "查找成功",list);
			}else{
				result=new ResultBean(ReslutConstants.STATUS_UNFOUND, "无数据");
			}
		}catch(Exception ex) {
			result=new ResultBean(ReslutConstants.STATUS_FAIL, "操作异常", ex.getMessage());
		}finally {
			try {
				ResultBeanWrite.writer(response, result);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
	}
}
