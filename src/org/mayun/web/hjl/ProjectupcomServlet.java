package org.mayun.web.hjl;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.mayun.common.ReslutConstants;
import org.mayun.common.ResultBean;
import org.mayun.common.ResultBeanWrite;
import org.mayun.server.hjl.ProjectupdateImpl;

/**
 * Servlet implementation class 修改项目内容
 */
@WebServlet("/project/update/content")
public class ProjectupcomServlet extends HttpServlet {
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ResultBean result=null;
		ProjectupdateImpl project=new ProjectupdateImpl();
		try {
			long user_id=Long.valueOf(request.getParameter("userId"));
			String language_id=request.getParameter("languageId");
			String project_id=request.getParameter("projectId");
			String category_id=request.getParameter("categoryId");
			String project_is_public=request.getParameter("projectIspublic");
			String project_name=request.getParameter("projectName");
			String project_exploit=request.getParameter("projectExploit");
			String project_comment=request.getParameter("projectComment");
			String project_version=request.getParameter("projectVersion");
			int i=project.update_project_comment(project_id, language_id, category_id, project_is_public, project_name, project_exploit, project_comment, project_version);
			if(i>0){
				result=new ResultBean(ReslutConstants.STATUS_SUCCESS, "项目修改成功");
			}else{
				result=new ResultBean(ReslutConstants.STATUS_UNFOUND, "项目修改失败");
			}
		}catch(Exception e) {
			result=new ResultBean(ReslutConstants.STATUS_FAIL,e.getMessage());
		}finally {
			try {
				ResultBeanWrite.writer(response, result);
			} catch (Exception e) {
				e.printStackTrace();
			}
		
		}
		
		
	}
}
