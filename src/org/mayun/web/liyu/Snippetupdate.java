package org.mayun.web.liyu;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.mayun.common.Comments;
import org.mayun.common.ReslutConstants;
import org.mayun.common.ResultBean;
import org.mayun.common.ResultBeanWrite;
import org.mayun.server.liyu.Project;
import org.mayun.server.liyu.ProjectComments;
import org.mayun.server.liyu.Snippet;


/**
 * Servlet implementation class Projectservlet
 */
@WebServlet("/Snippet/update")
public class Snippetupdate extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ResultBean result=null;
		try{
			String project_id=request.getParameter("snippetid");
			String language_id=request.getParameter("languageid");
			String category_id=request.getParameter("categoryid");
			String project_is_public=request.getParameter("snippetispublic");
			String project_name=request.getParameter("snippetname");
			String project_comment=request.getParameter("snippetcomment");
			String snippet_label=request.getParameter("snippetlabel");
			String snippet_intro=request.getParameter("snippetintro");
			Snippet snippet = new Snippet();
			int row =snippet.updateSnippet(project_id, language_id, category_id, project_is_public, project_name, project_comment,snippet_label,snippet_intro);
			if(row>0){
				result=new ResultBean(ReslutConstants.STATUS_SUCCESS, "插入成功");
			}else{
				result=new ResultBean(ReslutConstants.STATUS_UNFOUND, "插入失败");
			}
		}catch(Exception ex){
			result=new ResultBean(ReslutConstants.STATUS_FAIL, "插入异常", ex.getMessage());
		}finally{
			try {
				ResultBeanWrite.writer(response, result);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
    }
}
