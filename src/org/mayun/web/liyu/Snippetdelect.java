package org.mayun.web.liyu;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.mayun.common.Comments;
import org.mayun.common.ReslutConstants;
import org.mayun.common.ResultBean;
import org.mayun.common.ResultBeanWrite;
import org.mayun.server.liyu.Project;
import org.mayun.server.liyu.ProjectComments;
import org.mayun.server.liyu.Snippet;


/**
 * Servlet implementation class Projectservlet
 */
@WebServlet("/Snippet/delect")
public class Snippetdelect extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ResultBean result=null;
		try{
			String snippet_id=request.getParameter("snippetid");
			Snippet snippet = new Snippet();
			int row =snippet.delectSnippet(snippet_id);
			if(row>0){
				result=new ResultBean(ReslutConstants.STATUS_SUCCESS, "ɾ���ɹ�");
			}else{
				result=new ResultBean(ReslutConstants.STATUS_UNFOUND, "ɾ��ʧ��");
			}
		}catch(Exception ex){
			result=new ResultBean(ReslutConstants.STATUS_FAIL, "ɾ���쳣", ex.getMessage());
		}finally{
			try {
				ResultBeanWrite.writer(response, result);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
    }
}
