package org.mayun.web.liyu;



import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.mayun.common.ReslutConstants;
import org.mayun.common.ResultBean;
import org.mayun.common.ResultBeanWrite;
import org.mayun.server.liyu.projectUnForkServer;


/**
 * Servlet implementation class projectUnForkServlet
 */
@WebServlet("/projectUnFork")
public class projectUnForkServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	@Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String user_id=request.getParameter("user_id");
    	String project_id=request.getParameter("project_id");
    	boolean success=false;
    	projectUnForkServer projectUnForkServer=new projectUnForkServer();
    	success=projectUnForkServer.projectUnForkServer(Long.valueOf(user_id), project_id);
    	
    	ResultBean ResultBean=null;
    	
    	try {
       		if(success) {
        		ResultBean=new ResultBean(ReslutConstants.STATUS_SUCCESS, "验证通过");
        	}else {
        		ResultBean=new ResultBean(ReslutConstants.STATUS_UNFOUND, "验证未通过");
        	}
       	   	
       	}catch(Exception e) {
       		ResultBean=new ResultBean(ReslutConstants.STATUS_UNFOUND, "连接错误");
       	}
       	try {
    		ResultBeanWrite.writer(response, ResultBean);
    	} catch (Exception e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
    
    
    
    }


}
