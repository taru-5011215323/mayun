package org.mayun.web.liyu;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.mayun.common.ReslutConstants;
import org.mayun.common.ResultBean;
import org.mayun.common.ResultBeanWrite;
import org.mayun.server.liyu.OperationProject;
import org.mayun.server.liyu.Project;
import org.mayun.server.liyu.Snippet;


/**
 * 这个接口根据操作名称实现查询修改、删除
 * Servlet implementation class Projectservlet
 */
@WebServlet("/Project/operation/select")
public class OperationProjectservlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ResultBean result=null;
		try{
			//项目Id
			String OperationProjectId=request.getParameter("operationprojectid");
			//操作名
			String operation_name=request.getParameter("operationname");
			OperationProject operationProject=new OperationProject();
			//返回操作表
			List<HashMap<String,Object>> map=operationProject.listOperationProject(OperationProjectId, operation_name);
			if(map.size()>0){
				result=new ResultBean(ReslutConstants.STATUS_SUCCESS, "查询成功",map);
			}else{
				result=new ResultBean(ReslutConstants.STATUS_UNFOUND, "查询失败");
			}
		}catch(Exception ex){
			result=new ResultBean(ReslutConstants.STATUS_FAIL, "查询异常", ex.getMessage());
		}finally{
			try {
				ResultBeanWrite.writer(response, result);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
    }
}
