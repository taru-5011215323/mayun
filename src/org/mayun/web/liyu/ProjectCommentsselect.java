package org.mayun.web.liyu;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.mayun.common.Comments;
import org.mayun.common.ReslutConstants;
import org.mayun.common.ResultBean;
import org.mayun.common.ResultBeanWrite;
import org.mayun.server.liyu.Project;
import org.mayun.server.liyu.ProjectComments;


/**
 * Servlet implementation class Projectservlet
 */
@WebServlet("/Project/Comments/select")
public class ProjectCommentsselect extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ResultBean result=null;
		try{
			//项目Id
			String ProjectId=request.getParameter("projectid");
			ProjectComments projectComments = new ProjectComments();
			List<Comments> list =projectComments.Comments(Long.valueOf(ProjectId));
			//一个项目的所有评论及回复
			if(list.size()>0){
				result=new ResultBean(ReslutConstants.STATUS_SUCCESS, "查询成功",list);
			}else{
				result=new ResultBean(ReslutConstants.STATUS_UNFOUND, "查询失败");
			}
		}catch(Exception ex){
			result=new ResultBean(ReslutConstants.STATUS_FAIL, "查询异常", ex.getMessage());
		}finally{
			try {
				ResultBeanWrite.writer(response, result);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
    }
}
