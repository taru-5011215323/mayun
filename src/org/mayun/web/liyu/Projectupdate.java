package org.mayun.web.liyu;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.mayun.common.ReslutConstants;
import org.mayun.common.ResultBean;
import org.mayun.common.ResultBeanWrite;
import org.mayun.server.liyu.Project;


/**
 * Servlet implementation class Projectservlet
 */
@WebServlet("/Project/update")
public class Projectupdate extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ResultBean result=null;
		
			String project_id=request.getParameter("projectid");
			String language_id=request.getParameter("languageid");
			String category_id=request.getParameter("categoryid");
			String project_path=request.getParameter("projectpath");
			String project_name=request.getParameter("projectname");
			int project_exploit=Integer.parseInt(request.getParameter("projectexploit")) ;
			String project_comment=request.getParameter("projectcomment");

//			System.out.println(project_id);
//			System.out.println(language_id);
//			System.out.println(category_id);
//			System.out.println(project_path);
//			System.out.println(project_name);
//			System.out.println(project_exploit);
//			System.out.println(project_comment);
			try{
				Project project = new Project();
			int row =project.updateProject(project_id, language_id, category_id, project_path, project_name, project_exploit, project_comment);
			if(row>0){
				result=new ResultBean(ReslutConstants.STATUS_SUCCESS, "修改成功");
			}else{
				result=new ResultBean(ReslutConstants.STATUS_UNFOUND, "修改失败");
			}
		}catch(Exception ex){
			result=new ResultBean(ReslutConstants.STATUS_FAIL, "修改异常", ex.getMessage());
		}finally{
			try {
				ResultBeanWrite.writer(response, result);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
    }
}
