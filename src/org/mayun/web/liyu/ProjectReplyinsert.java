package org.mayun.web.liyu;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.mayun.common.Comments;
import org.mayun.common.ReslutConstants;
import org.mayun.common.ResultBean;
import org.mayun.common.ResultBeanWrite;
import org.mayun.server.liyu.Project;
import org.mayun.server.liyu.ProjectComments;


/**
 * Servlet implementation class Projectservlet
 */
@WebServlet("/Project/Reply/insert")
public class ProjectReplyinsert extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ResultBean result=null;
		try{
			String ReplyUserId=request.getParameter("replyuserid");
			String CommentId=request.getParameter("commentid");
			String Comment=request.getParameter("comment");
			ProjectComments projectComments = new ProjectComments();
			int row =projectComments.insertProjectReply(ReplyUserId, CommentId, Comment);
			if(row>0){
				result=new ResultBean(ReslutConstants.STATUS_SUCCESS, "插入成功");
			}else{
				result=new ResultBean(ReslutConstants.STATUS_UNFOUND, "插入失败");
			}
		}catch(Exception ex){
			result=new ResultBean(ReslutConstants.STATUS_FAIL, "插入异常", ex.getMessage());
		}finally{
			try {
				ResultBeanWrite.writer(response, result);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
    }
}
