package org.mayun.web.linzeyv;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.mayun.common.ReslutConstants;
import org.mayun.common.ResultBean;
import org.mayun.common.ResultBeanWrite;
import org.mayun.server.linzeyv.selectLaSnippetNaServiceImpl;

/**
 * 根据代码片段名和语言查询
* @author lzy
*
*/
@WebServlet("/api/selectlanguagesnippetname")
public class selectLaSnippetNaServlet extends HttpServlet {
@Override


protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	String language=request.getParameter("language");
	String snippetName="%"+request.getParameter("snippetName")+"%";
	int pagenum=Integer.parseInt(request.getParameter("pagenum"));
	
	ResultBean result =null;
	try {
		selectLaSnippetNaServiceImpl serviceImol=new selectLaSnippetNaServiceImpl();
		List<HashMap<String,Object>> list=serviceImol.selectlanguagesnippetname(language, snippetName,pagenum);
		if(list!=null) {
			result=new ResultBean(ReslutConstants.STATUS_SUCCESS, "查询成功", list);
		}else {
			result=new ResultBean(ReslutConstants.STATUS_UNFOUND, "查询失败");
		}
	}catch(Exception ex) {
		result=new ResultBean(ReslutConstants.STATUS_FAIL, "查询异常",ex.getMessage());
	}
	
	try {
		ResultBeanWrite.writer(response, result);
	} catch (Exception e) {
		e.printStackTrace();
	}
	
	
}

}
