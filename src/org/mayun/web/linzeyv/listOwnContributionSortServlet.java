package org.mayun.web.linzeyv;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.mayun.common.ReslutConstants;
import org.mayun.common.ResultBean;
import org.mayun.common.ResultBeanWrite;
import org.mayun.server.linzeyv.listAllContributionSortServiceImpl;
import org.mayun.server.linzeyv.listOwnContributionSortServiceImpl;

/**
 * (动态) 自己的操作动态  限制条件用户id  userid
	 * 根据时间排序显示 倒序
	 * 显示操作人和具体的操作对象
* @author lzy
*
*/
@WebServlet("/api/owncontribution")
public class listOwnContributionSortServlet extends HttpServlet {
@Override


protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
	String operationUserId=request.getParameter("operationUserId");
	
	ResultBean result =null;
	try {
		listOwnContributionSortServiceImpl serviceImol=new listOwnContributionSortServiceImpl();
		List<HashMap<String,Object>> list=serviceImol.listowncontributionsort(operationUserId);
		if(list!=null) {
			result=new ResultBean(ReslutConstants.STATUS_SUCCESS, "查询成功", list);
		}else {
			result=new ResultBean(ReslutConstants.STATUS_UNFOUND, "查询失败");
		}
	}catch(Exception ex) {
		result=new ResultBean(ReslutConstants.STATUS_FAIL, "查询异常",ex.getMessage());
	}
	
	try {
		ResultBeanWrite.writer(response, result);
	} catch (Exception e) {
		e.printStackTrace();
	}
	
	
}

}