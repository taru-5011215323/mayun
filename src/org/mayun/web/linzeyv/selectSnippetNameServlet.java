package org.mayun.web.linzeyv;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.mayun.common.ReslutConstants;
import org.mayun.common.ResultBean;
import org.mayun.common.ResultBeanWrite;
import org.mayun.server.linzeyv.selectSnippetNameServiceImpl;

/**
 * 根据代码片段名查询
* @author lzy
*
*/
@WebServlet("/api/selectsnippetname")
public class selectSnippetNameServlet extends HttpServlet {
@Override


protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	String snippetName="%"+request.getParameter("snippetName")+"%";
//	String pagenum=request.getParameter("pagenum");
	int pagenum=Integer.parseInt(request.getParameter("pagenum"));
	System.out.println(snippetName);
	System.out.println(pagenum);
	
	ResultBean result =null;
	try {
		selectSnippetNameServiceImpl serviceImol=new selectSnippetNameServiceImpl();
		List<HashMap<String,Object>> list=serviceImol.selectSnippetName(snippetName,pagenum);
		if(list!=null) {
			result=new ResultBean(ReslutConstants.STATUS_SUCCESS, "查询成功", list);
		}else {
			result=new ResultBean(ReslutConstants.STATUS_UNFOUND, "查询失败");
		}
	}catch(Exception ex) {
		result=new ResultBean(ReslutConstants.STATUS_FAIL, "查询异常",ex.getMessage());
	}
	
	try {
		ResultBeanWrite.writer(response, result);
	} catch (Exception e) {
		e.printStackTrace();
	}
	
	
}

}
