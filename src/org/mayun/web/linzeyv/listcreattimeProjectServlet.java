package org.mayun.web.linzeyv;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.mayun.common.ReslutConstants;
import org.mayun.common.ResultBean;
import org.mayun.common.ResultBeanWrite;
import org.mayun.server.linzeyv.listRecommendableProjectsServiceImpl;
import org.mayun.server.linzeyv.listcreattimeProjectServiceImpl;

/**
 * 推荐项目   
 * 根据语言和类别来推荐 传参为 language 和catgory
 * 特别提醒 传特殊符号%号时 用  %25 代替  URL对特殊符号有限制
 * @author lzy
 *
 */

@WebServlet("/api/listcreattimeProject")
public class listcreattimeProjectServlet extends HttpServlet {
	@Override
	
	
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String language=request.getParameter("language");
		String catgory=request.getParameter("catgory");
		int pagenum=Integer.parseInt(request.getParameter("pagenum"));
		
		ResultBean result =null;
		try {
			listcreattimeProjectServiceImpl serviceImol=new listcreattimeProjectServiceImpl();
			List<HashMap<String,Object>> list= serviceImol.listRessss(language, catgory, pagenum);
			if(list!=null) {
				result=new ResultBean(ReslutConstants.STATUS_SUCCESS, "查询成功", list);
			}else {
				result=new ResultBean(ReslutConstants.STATUS_UNFOUND, "查询失败");
			}
		}catch(Exception ex) {
			result=new ResultBean(ReslutConstants.STATUS_FAIL, "查询异常",ex.getMessage());
		}
		
		try {
			ResultBeanWrite.writer(response, result);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}

}