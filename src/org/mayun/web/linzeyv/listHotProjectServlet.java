package org.mayun.web.linzeyv;

import java.io.IOException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.mayun.common.ReslutConstants;
import org.mayun.common.ResultBean;
import org.mayun.common.ResultBeanWrite;
import org.mayun.server.linzeyv.listHotProjectServiceImpl;

/**
 * 根据浏览量查询排名前五的项目 
 * 查询用户自己的     传参 userid
 * @author lzy
 *
 */
@WebServlet("/api/hotproject")
public class listHotProjectServlet extends HttpServlet {
	@Override
	
	
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String userid=request.getParameter("userid");
		
		
		ResultBean result =null;
		try {
			listHotProjectServiceImpl serviceImol=new listHotProjectServiceImpl();
			List<HashMap<String,Object>> list=serviceImol.listHotProject(userid);
			if(list!=null) {
				result=new ResultBean(ReslutConstants.STATUS_SUCCESS, "查询成功", list);
			}else {
				result=new ResultBean(ReslutConstants.STATUS_UNFOUND, "查询失败");
			}
		}catch(Exception ex) {
			result=new ResultBean(ReslutConstants.STATUS_FAIL, "查询异常",ex.getMessage());
		}
		
		try {
			ResultBeanWrite.writer(response, result);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}

}
