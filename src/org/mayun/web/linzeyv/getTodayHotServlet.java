package org.mayun.web.linzeyv;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.mayun.common.ReslutConstants;
import org.mayun.common.ResultBean;
import org.mayun.common.ResultBeanWrite;
import org.mayun.server.linzeyv.getTodayHotServiceImpl;

/**
 * 今日热门   
 * 时间限制今天新建的项目  根据浏览量排序
 * @author lzy
 *
 */
@WebServlet("/api/todayhost")
public class getTodayHotServlet extends HttpServlet {
	@Override
	
	
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		ResultBean result =null;
		try {
			getTodayHotServiceImpl serviceImol=new getTodayHotServiceImpl();
			List<HashMap<String,Object>> list=serviceImol.getTodayHotService();
			if(list!=null) {
				result=new ResultBean(ReslutConstants.STATUS_SUCCESS, "查询成功", list);
			}else {
				result=new ResultBean(ReslutConstants.STATUS_UNFOUND, "查询失败");
			}
		}catch(Exception ex) {
			result=new ResultBean(ReslutConstants.STATUS_FAIL, "查询异常",ex.getMessage());
		}
		
		try {
			ResultBeanWrite.writer(response, result);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}

}

