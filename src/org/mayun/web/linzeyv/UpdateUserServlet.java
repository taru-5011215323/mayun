package org.mayun.web.linzeyv;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.mayun.common.ReslutConstants;
import org.mayun.common.ResultBean;
import org.mayun.common.ResultBeanWrite;
import org.mayun.server.linzeyv.UpdateUserServiceImpl;

/**
 * 用户信息
 * @author lzy
 *
 */
@WebServlet("/api/updateuser")
public class UpdateUserServlet extends HttpServlet {
	@Override
	
	
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String userid=request.getParameter("userid") ;
		String userrealname=request.getParameter("userrealname");
		String usertelephone=request.getParameter("usertelephone");
		String userlocation=request.getParameter("userlocation");
		String userdetailed=request.getParameter("userdetailed");
		
		ResultBean result =null;
		try {
			UpdateUserServiceImpl serviceImol=new UpdateUserServiceImpl();
			int list=serviceImol.updateuserdao(userid, userrealname, usertelephone, userlocation, userdetailed);
			if(list>0) {
				result=new ResultBean(ReslutConstants.STATUS_SUCCESS, "修改成功", list);
			}else {
				result=new ResultBean(ReslutConstants.STATUS_UNFOUND, "修改失败");
			}
		}catch(Exception ex) {
			result=new ResultBean(ReslutConstants.STATUS_FAIL, "修改异常",ex.getMessage());
		}
		
		try {
			ResultBeanWrite.writer(response, result);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}

}
